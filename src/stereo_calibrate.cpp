//
// Created by tibo on 20/10/17.
//

#include <iostream>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
using namespace boost::accumulators;

#include <boost/range/numeric.hpp>
#include <boost/range/adaptors.hpp>

#include <opencv2/core/core.hpp>

#include <dvrk_camera_calibration/error.hpp>
#include <dvrk_camera_calibration/cv_types.hpp>
#include <dvrk_camera_calibration/calibration_dataset.hpp>
#include <dvrk_camera_calibration/stereo_calibration_dataset.hpp>

#include "common.hpp"

int main(int argc, char** argv) {
    
    po::positional_options_description positional_opts;
    positional_opts.add("config-file", 1);
    
    po::options_description main_opts("Options");
    main_opts.add_options()
                 ("help", "Show this help.")
                 (
                     "config-file,c",
                     po::value<std::string>()->required(),
                     "The configuration file to use. See the README for more information"
                 ) (
        "output_suffix,s",
        po::value<std::string>()->default_value("stereo"),
        "The suffix to add to the names of the original calibration files when saving the new ones."
    );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .positional(positional_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: required option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    // Load the configuration file
    const auto config_file = fs::absolute(cmdline["config-file"].as<std::string>());
    if (!fs::is_regular_file(config_file)) {
        std::cerr << config_file.string() << " is not a regular file. Cannot proceed." << std::endl;
        exit(-1);
    }
    
    cv::FileStorage cfg(config_file.string(), cv::FileStorage::READ);
    if (!cfg.isOpened()) {
        std::cerr << "Unable to open configuration file " << config_file.string() << std::endl;
        exit(-1);
    }
    
    try {
        // Parse the configuration file
        dvrk::CalibrationPattern pattern;
        const auto pattern_node = cfg["pattern"];
        if (!pattern_node.empty()) {
            pattern = dvrk::CalibrationPattern::FromFileStorage(pattern_node);
        } else {
            std::cerr << "Could not locate 'pattern' node in the configuration file. Unable to "
                      << "continue." << std::endl;
            exit(-1);
        }
    
        fs::path base_path = config_file.parent_path();
        std::cout << "Loading images for left camera ...\n";
        dvrk::CalibrationDataset left_imageset = load_images(cfg["left"], pattern, base_path);
        std::cout << "\nLoading images for right camera ...\n";
        dvrk::CalibrationDataset right_imageset = load_images(cfg["right"], pattern, base_path);
        
        dvrk::StereoCalibrationDataset imageset(
            left_imageset.get_images(), right_imageset.get_images());
        std::cout << "\n";
    
        const auto calibration_opts_node = cfg["calibration_flags"];
        bool fix_intrinsics = false;
    
        if (!calibration_opts_node.empty()) {
            if (!calibration_opts_node.isSeq()) {
                std::cerr
                    << "'calibration_flags' node in the configuration file should be a list of option names."
                    << std::endl;
                exit(-1);
            }
        
            std::cout << "Using the following calibration flags: \n";
        
            int calibration_opts = 0;
            for (const auto& node: calibration_opts_node) {
                try {
                    dvrk::CalibFlags opt = dvrk::to_CalibFlags(static_cast<std::string>(node));
                    if (opt == dvrk::CalibFlags::FixIntrinsics) {
                        fix_intrinsics = true;
                    }
    
                    std::cout << "  - " << dvrk::to_string(opt) << "\n";
                    calibration_opts = static_cast<int>(opt);
                } catch (const std::out_of_range &ex) {
                    std::cerr << "Error: '" << ex.what() << "' is not a valid value for 'calibration_flags'\n";
                    exit(-1);
                }
            }
            
            std::cout << std::endl;
            imageset.set_calibration_flags(calibration_opts);
        }
        
        const auto& left_calib_node = cfg["left_camera_calibration_file"];
        const auto& right_calib_node = cfg["right_camera_calibration_file"];
        
        if (fix_intrinsics and (left_calib_node.empty() or right_calib_node.empty())) {
            std::cerr << "You passed the 'FixIntrinsics' option but failed to specify calibration files for the left and right cameras in the configuration file.\n";
            exit(-1);
        }
        
        // We now know that either both calibration files are specified
        if (!left_calib_node.empty()) {
            fs::path left_calib_file{ left_calib_node.string() };
            if (left_calib_file.is_relative())
                left_calib_file = config_file.parent_path() / left_calib_file;
            
            try {
                std::cout << "Loading left camera calibration file " << left_calib_file << " ...\n";
                imageset.set_left_camera_calibration(dvrk::CameraParams::FromFile(left_calib_file));
            } catch (const dvrk::file_error& ex) {
                std::cerr << "Unable to open calibration file " << left_calib_file << "\n" << boost::diagnostic_information(ex) << "\n";
                exit(-1);
            }
        }
    
        if (!right_calib_node.empty()) {
            fs::path right_calib_file{ right_calib_node.string() };
            if (right_calib_file.is_relative())
                right_calib_file = config_file.parent_path() / right_calib_file;
            
            try {
                std::cout << "Loading right camera calibration file " << right_calib_file << " ...\n";
                imageset.set_right_camera_calibration(dvrk::CameraParams::FromFile(right_calib_file));
            } catch (const dvrk::file_error& ex) {
                std::cerr << "Unable to open calibration file " << right_calib_file << "\n" << boost::diagnostic_information(ex) << "\n";
                exit(-1);
            }
        }
        
        std::cout << "Computing optimal camera calibration ...\n";
        imageset.calibrate();
        std::cout << "\n";
        
        fs::path left_intrinsics_file;
        if (!left_calib_node.empty()) {
            const auto original_calib_file =
                fs::absolute(std::string{ cfg["left_camera_calibration_file"].string() });
            left_intrinsics_file = original_calib_file.stem().string() + "-stereo.yaml";
        } else {
            left_intrinsics_file = fs::absolute("stereo-camera-left.yaml");
        }
        std::cout << "Saving complete calibration results (including rectification and projection matrices) for the left camera to " << left_intrinsics_file << " ...\n";
    
        fs::path right_intrinsics_file;
        if (!right_calib_node.empty()) {
            const auto original_calib_file =
                 fs::absolute(std::string{ cfg["right_camera_calibration_file"].string() });
            
            right_intrinsics_file = original_calib_file.stem().string() + std::string("-stereo.yaml");
        } else {
            right_intrinsics_file = fs::absolute("stereo-camera-right.yaml");
        }
        std::cout << "Saving complete calibration results (including rectification and projection matrices) for the right camera to " << right_intrinsics_file << " ...\n";
        
        imageset.save_calibration(left_intrinsics_file, right_intrinsics_file);
        
    } catch (const boost::exception& ex) {
        std::cerr << "Unexpected error:\n"
                  << boost::diagnostic_information(ex) << std::endl;
    } catch (const std::exception& ex) {
        std::cerr << "Unexpected error: " << ex.what() << std::endl;
    }
    
    return 0;
}
