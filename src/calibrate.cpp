//
// Created by tibo on 20/10/17.
//

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
using namespace boost::accumulators;

#include <boost/range/numeric.hpp>
#include <boost/range/adaptors.hpp>

#include <opencv2/core/core.hpp>

#include <dvrk_camera_calibration/error.hpp>
#include <dvrk_camera_calibration/cv_types.hpp>
#include <dvrk_camera_calibration/calibration_dataset.hpp>

#include "common.hpp"

int main(int argc, char** argv) {
   
    po::positional_options_description positional_opts;
    positional_opts.add("config-file", 1);
    
    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help.")
        (
            "config-file,c",
            po::value<std::string>()->required(),
            "The configuration file to use. See the README for more information"
        ) (
            "iteration-parameters-file,d",
            po::value<std::string>()->default_value("calibration_details.csv"),
            "The file in which to save the detailed progression of the calibration"
        ) (
            "reprojection-errors-file,r",
            po::value<std::string>()->default_value("calibration_details_extra.csv"),
            "The file in which to save the very detailed progression of the calibration"
        ) (
            "calibration-results,r",
            po::value<std::string>()->default_value(""),
            "The file in which to save the intrinsic camera calibration parameters"
        );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .positional(positional_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: required option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    // Load the configuration file
    const auto config_file = fs::absolute(cmdline["config-file"].as<std::string>());
    if (!fs::is_regular_file(config_file)) {
        std::cerr << config_file.string() << " is not a regular file. Cannot proceed." << std::endl;
        exit(-1);
    }
    
    cv::FileStorage cfg;
    try {
        cfg.open(config_file.string(), cv::FileStorage::READ);
        if (!cfg.isOpened()) {
            std::cerr << "Unable to open configuration file " << config_file.string() << std::endl;
            exit(-1);
        }
    } catch (const cv::Exception &ex) {
        std::cerr << "Unable to parse configuration file:\n => " << ex.err << std::endl;
        exit(-1);
    }
    
    fs::path calibration_file;
    if (cmdline["calibration-results"].defaulted()) {
        if (!cfg["camera_name"].isNone()) {
            calibration_file = fs::absolute(static_cast<std::string>(cfg["camera_name"]) + ".yaml");
        } else {
            calibration_file = "camera-intrinsics.yaml";
        }
    } else {
        calibration_file = fs::absolute(cmdline["calibration-results"].as<std::string>());
    }
    
    fs::path details_file;
    if (cmdline["iteration-parameters-file"].defaulted()) {
        if (!cfg["camera_name"].isNone()) {
            details_file = fs::absolute(
                static_cast<std::string>(cfg["camera_name"]) + "-iteration-parameters.csv" );
        } else {
            details_file = "detailed_calibration_results.csv";
        }
    } else {
        details_file = fs::absolute(cmdline["iteration-parameters-file"].as<std::string>());
    }

    
    fs::path extra_details_file;
    if (cmdline["reprojection-errors-file"].defaulted()) {
        if (!cfg["camera_name"].isNone()) {
            extra_details_file = fs::absolute(
                static_cast<std::string>(cfg["camera_name"]) + "-per-image-reprojection-errors.csv" );
        } else {
            extra_details_file = "per-image-reprojection-errors.csv";
        }
    } else {
        extra_details_file = fs::absolute(cmdline["reprojection-errors-file"].as<std::string>());
    }
    
    
    try {
        // Parse the configuration file
        dvrk::CalibrationPattern pattern;
        const auto pattern_node = cfg["pattern"];
        if (!pattern_node.isNone()) {
            pattern = dvrk::CalibrationPattern::FromFileStorage(pattern_node);
        } else {
            std::cerr << "Could not locate 'pattern' node in the configuration file. Unable to "
                      << "continue." << std::endl;
            exit(-1);
        }
        
        const auto imageset_node = cfg["images"];
        fs::path base_path = config_file.parent_path();
    
        std::cout << "Loading images ...\n";
        dvrk::CalibrationDataset imageset = load_images(imageset_node, pattern, base_path);
        
        const auto calibration_opts_node = cfg["calibration_flags"];
        if (!calibration_opts_node.isNone()) {
            if (!calibration_opts_node.isSeq()) {
                std::cerr
                    << "'calibration_flags' node in the configuration file should be a list of option names."
                    << std::endl;
                exit(-1);
            }
            
            std::cout << "\nUsing the following calibration flags: \n";
            
            int calibration_opts = 0;
            for (const auto& node: calibration_opts_node) {
                dvrk::CalibFlags opt = dvrk::to_CalibFlags(static_cast<std::string>(node));
                std::cout << "  - " << dvrk::to_string(opt) << "\n";
                
                calibration_opts = static_cast<int>(opt);
            }
            
            imageset.set_calibration_flags(calibration_opts);
        }
    
        std::cout << "\nStarting calibration procedure ...\n";
        imageset.calibrate();
        
        std::cout << "\nSaving calibration results to " << calibration_file << " ...\n";
        imageset.save_calibration(calibration_file);
        
        std::cout << "Saving per iteration parameters to " << details_file.string() << " ...\n";
        imageset.save_details(details_file);
        
        std::cout << "Saving per image reprojection errors to " << extra_details_file.string() << " ...\n";
        imageset.save_extra_details(extra_details_file);
        
        std::cout << "Done.\n";
    
    } catch (const boost::exception& ex) {
        std::cerr << "Unexpected error:\n"
                  << boost::diagnostic_information(ex) << std::endl;
    } catch (const std::exception& ex) {
        std::cerr << "Unexpected error: " << ex.what() << std::endl;
    }
    
    return 0;
}
