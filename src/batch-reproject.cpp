//
// Created by tibo on 22/12/17.
//

#include <fstream>
#include <iostream>

#include <opencv2/imgcodecs.hpp>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <dvrk_camera_calibration/util.hpp>
#include <dvrk_camera_calibration/camera_params.hpp>
#include <dvrk_camera_calibration/calibration_pattern.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>


int main(int argc, char** argv) {
    try {
        po::positional_options_description positional_opts;
        positional_opts.add("images", 1);
    
        po::options_description main_opts("Options");
        main_opts.add_options()
        ("help", "Show this help.")
        (
            "images,i",
            po::value<fs::path>()->multitoken()->required(),
            "The folder from which to load the input images."
        )(
            "output-folder,o",
            po::value<fs::path>(),
            "The folder in which to write the ouput images."
        )(
            "intrinsics-file,f",
            po::value<fs::path>()->required(),
            "The file from which to rread the intrinsic camera calibration parameters"
        )(
            "pattern,p", 
            po::value<std::string>()->required(), 
            "The pattern spec formatted as type:width:height:feature-size"
        );
    
        po::variables_map cmdline;
        try {
            auto parsed_opts = po::command_line_parser(argc, argv)
                .options(main_opts)
                .positional(positional_opts)
                .run();
        
            po::store(parsed_opts, cmdline);
            if (cmdline.count("help")) {
                std::cout << main_opts << std::endl;
                exit(-1);
            }
        
            po::notify(cmdline);
        }
        catch (po::required_option& ex) {
            std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                      << "\" missing from command line arguments" << std::endl;
            exit(-1);
        }
        catch (po::unknown_option& ex) {
            std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                      << std::endl;
            exit(-1);
        }
    
        const auto pattern_spec = cmdline["pattern"].as<std::string>();
        const auto pattern = dvrk::CalibrationPattern::FromString(pattern_spec);
    
        std::cout << "Using " << pattern.size.width << "x" << pattern.size.height << " "
                  << "chessboard target with " << pattern.feature_size * 1000 << "mm features.\n\n";
    
        const auto intrinsics_file = fs::absolute(cmdline["intrinsics-file"].as<fs::path>());
        std::cout << "Loading camera intrinsics from " << intrinsics_file << " ...\n";
        dvrk::CameraParams camera_params = dvrk::CameraParams::FromFile(intrinsics_file);
    
        std::vector<fs::path> images;
        const auto image_path = cmdline["images"].as<fs::path>();
        if (fs::is_directory(image_path)) {
            std::cout << "Scanning folder " << image_path << " for images ...\n";
            
            for (const auto& image: fs::directory_iterator(image_path)) {
                if (!dvrk::is_image_file(image.path())) {
                    std::cout << " - Skipping " << image.path() << ", not a known image type\n";
                    continue;
                }
                
                std::cout << " - Adding " << image.path() << "\n";
                images.push_back(image.path());
            }
        } else if (dvrk::is_image_file(image_path)) {
            std::cout << " - Adding " << image_path << "\n";
            images.push_back(image_path);
        } else {
            std::cerr << "Error: " << image_path << " is not an image file or a folder.\n";
            return 1;
        }
        std::sort(images.begin(), images.end());
        
        if (images.empty()) {
            std::cerr << "No images read. Nothing to do.\n";
            return 1;
        }
    
        fs::path output_dir{"reprojected-images"};
        if (cmdline.count("output-folder")) {
            output_dir = cmdline["output-folder"].as<fs::path>();
        }
        
        if (fs::exists(output_dir)) {
            if (!fs::is_directory(output_dir)) {
                std::cerr << "Error: output directory " << output_dir.string()
                          << " already exists but is not a directory.\n";
                return 1;
            }
        }
        else {
            std::cout << "Creating output directory " << output_dir.string() << "...\n";
            fs::create_directory(output_dir);
        }
        
        std::cout << "Loading images ...\n";
        std::vector<std::pair<std::string, float>> reprojection_errors;
        for (const auto this_image : images) {
            std::cout << "- Processing image " << this_image << " ...\n";
            dvrk::CalibrationImage image(this_image, pattern);
            if (!image.hasChessboard()) {
                std::cout << "   -> Warning: Chessboard not found. Skipping\n";
                continue;
            }
            
            image.set_intrinsics(camera_params.camera_matrix, camera_params.distortion_coefficients);
            cv::Mat reprojected_image = image.show_points();
            reprojection_errors.emplace_back(image.getName(), image.get_reprojection_error_mm());
        
            fs::path full_output_path = output_dir / this_image.filename();
            std::cout << "   -> Writing output to " << full_output_path << " ...\n";
        
            if (!cv::imwrite(full_output_path.string(), reprojected_image)) {
                std::cerr << "Error: Unable to write image to " << full_output_path << std::endl;
                return 1;
            }
        }
    
        fs::path csv_summary = output_dir / "per-image-reprojection-errors.csv";
        std::ofstream csv(csv_summary.string());
    
        if (!csv) {
            std::cerr << "Error: Unable to write reprojection errors summary to " << csv_summary
                      << std::endl;
            return 1;
        }
    
        csv << "Image Name, Reprojection Error (mm)\n";
        for (const auto kv : reprojection_errors) {
            csv << "\"" << kv.first << "\", " << kv.second << "\n";
        }
        csv.close();
    
        return 0;
    } catch (const boost::exception& ex) {
        std::cerr << "Ooops! An unexpected error occurred. \n\n" << boost::diagnostic_information(ex, true) << std::endl;
        return 1;
    }
}
