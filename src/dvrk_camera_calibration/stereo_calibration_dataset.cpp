//
// Created by tibo on 13/11/17.
//

#include <iostream>
#include <opencv2/calib3d.hpp>

#include "dvrk_camera_calibration/stereo_calibration_dataset.hpp"


dvrk::StereoCalibrationDataset::StereoCalibrationDataset(
    const std::vector<dvrk::CalibrationImage>& left_images,
    const std::vector<dvrk::CalibrationImage>& right_images
) : _left_images(left_images), _right_images(right_images)
{
    if (_left_images.size() != _right_images.size()) {
        BOOST_THROW_EXCEPTION(runtime_error() << errmsg("Left and right image sets are different sizes"));
    }
    
    // Use a sane set of defaults for the calibration, This helps to reduce the dimensionality of
    // the problem. If you can calibrate the intrinsics separately you should.
    // Can be overriden with set_calibration_flags(int ...)
    _calibration_flags = cv::CALIB_FIX_ASPECT_RATIO + cv::CALIB_ZERO_TANGENT_DIST
                       + cv::CALIB_RATIONAL_MODEL + cv::CALIB_FIX_K3 + cv::CALIB_FIX_K4
                       + cv::CALIB_FIX_K5;
}


void dvrk::StereoCalibrationDataset::calibrate() {
    std::vector<std::vector<cv::Point2f>> left_image_points, right_image_points;
    std::vector<std::vector<cv::Point3f>> object_points;
    
    
    for(const auto& image: _left_images) {
        object_points.push_back(image.getObjectPoints());
        left_image_points.push_back(image.getImagePoints());
    }
    
    for(const auto& image: _right_images) {
        right_image_points.push_back(image.getImagePoints());
    }
    
    // Assume horizontal stereo
    cv::Mat R, T, E, F; // Camera Pose (R, T), Essential, Fundamental
    cv::Size imsize{ _left_images.front().size() };
    double rms = cv::stereoCalibrate(
        /* Inputs */
        object_points, left_image_points, right_image_points,
        _left_camera_params.camera_matrix, _left_camera_params.distortion_coefficients,
        _right_camera_params.camera_matrix, _right_camera_params.distortion_coefficients,
        imsize,
        
        /* Outputs */
        R, T, E, F,
        
        /* Configuration Options */
        _calibration_flags,
        cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 100, 1e-5)
    );

    std::cout << "\n"
              << "Stereo calibration RMS error: " << rms << "\n"
              << "Left to Right rotation matrix: [\n"
              << "  " << R.at<double>(0, 0) << ", " << R.at<double>(0, 1) << ", " << R.at<double>(0, 2) << "\n"
              << "  " << R.at<double>(1, 0) << ", " << R.at<double>(1, 1) << ", " << R.at<double>(1, 2) << "\n"
              << "  " << R.at<double>(2, 0) << ", " << R.at<double>(2, 1) << ", " << R.at<double>(2, 2) << "\n"
              << "]\n"
              << "Left to Right translation: " << T << "\n"; 

    // TODO: compute epi-polar error
    
    cv::Mat Q; // Rectification (R1, R2), Projection (p1, P2)
    cv::stereoRectify(
        /* Inputs */
        _left_camera_params.camera_matrix, _left_camera_params.distortion_coefficients,
        _right_camera_params.camera_matrix, _right_camera_params.distortion_coefficients,
        imsize, R, T,
        
        _left_camera_params.rectification_matrix, _right_camera_params.rectification_matrix,
        _left_camera_params.projection_matrix, _right_camera_params.projection_matrix,
        Q
    );
    
    _left_camera_params.stereo_frame_count = _left_images.size();
}


void dvrk::StereoCalibrationDataset::save_calibration(
    const fs::path& left_filename,
    const fs::path& right_filename
) {
    _left_camera_params.to_file(left_filename);
    _right_camera_params.to_file(right_filename);
}


void dvrk::StereoCalibrationDataset::set_left_camera_calibration(const CameraParams& left_camera_params) {
    _left_camera_params = left_camera_params;
}


void dvrk::StereoCalibrationDataset::set_right_camera_calibration(const CameraParams& right_camera_params) {
    _right_camera_params = right_camera_params;
}




