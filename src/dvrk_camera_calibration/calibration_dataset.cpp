//
// Created by nearlab on 25/10/17.
//

#include <fstream>
#include <iostream>

#include <boost/range/adaptors.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
using namespace boost::accumulators;

#include <opencv2/calib3d.hpp>

#include "dvrk_camera_calibration/calibration_dataset.hpp"


dvrk::CalibrationDataset::CalibrationDataset(double threshold)
    : _min_iteration_change(threshold)
{
    // Do Nothing
}


dvrk::CalibrationDataset::CalibrationDataset(const std::vector<CalibrationImage>& images, double threshold)
    : _min_iteration_change(threshold), _initial_imageset(images)
{
    // Do nothing
}


void dvrk::CalibrationDataset::add(CalibrationImage image)
{
    if (!image.hasChessboard())
        BOOST_THROW_EXCEPTION(runtime_error() << errmsg("Image does not have a chessboard."));
    
    if (!_initial_imageset.empty()) {
        const auto imsize = _initial_imageset.front().size();
        if (image.size() != imsize) {
            BOOST_THROW_EXCEPTION(size_error()
                << errmsg("New image does not have the same size as the image already in the calibration set"));
        }
    }
    
    _initial_imageset.push_back(std::move(image));
}


void dvrk::CalibrationDataset::calibrate() {
    if (_initial_imageset.empty()) {
        BOOST_THROW_EXCEPTION(runtime_error() << errmsg("Cannot calibrate with an empty imageset"));
    }
    
    // Images that are still part of the calibration dataset
    auto images_in_use = _initial_imageset;
    
    // initialise the "all_errors" data
    _all_reprojection_errors.clear();
    for (const auto image : images_in_use) {
        _all_reprojection_errors[image.getName()] = {};
    }
    
    // Run once and discard all the outliers, images with a high reprojection error
    cv::Mat camera_matrix, distortion_coeffs;
    
    std::vector<CalibrationImage> new_imageset;
    size_t discarded_image_count = 0;
    
    if (images_in_use.size() > _min_imageset_size) {
        std::cout << "Performing preliminary filtering, removing outliers ...\n";
        std::vector<double> errors = iterate_once(images_in_use, camera_matrix, distortion_coeffs);
        update_error_stats(images_in_use, errors, camera_matrix);
        show_iteration_stats(images_in_use);
        
        for(size_t i = 0; i < errors.size(); ++i) {
            if (errors[i] < _hard_error_cutoff) {
                new_imageset.push_back(std::move(images_in_use.at(i)));
            }
            else {
                std::cout << "   -> Discarding <" << images_in_use.at(i).getName()
                          << "> with a reprojection error of " << errors[i] << "px\n";
                discarded_image_count += 1;
            }
        }
    
        images_in_use = new_imageset;
    
        if (new_imageset.empty()) {
            std::cout << "WARNING: All the input images were discarded in the initial filtering pass because they have re-projection errors greater than 1.\n"
                      << "This may be an indication that your images are low quality which might affect the accuracy of the calibration.\n"
                      << "Restoring all images in order to proceed ...\n";
            images_in_use = _initial_imageset;
        }
    
    }
    
    if (discarded_image_count > 0) {
        std::cout << "   -> Discarded " << discarded_image_count << " outlier images\n";
    }
    
    std::cout << "\nOptimising calibration ...\n";
    std::vector<double> previous_errors;
    do {
        // Remove the worst performing image at each iterations
        if (_mean_errors.size() > 1) {
            // Discard the image with the highest reprojection error
            const auto worst = std::max_element(previous_errors.begin(), previous_errors.end());
            const auto worst_idx = std::distance(previous_errors.begin(), worst);
            const CalibrationImage& worst_image = _initial_imageset.at(worst_idx);
            std::cout << "   -> Discarding <" << worst_image.getName()
                      << "> with reprojection error of "
                      << *worst << "px.\n";
    
            // Remove the worst image from the working set
            images_in_use.erase(images_in_use.begin() + worst_idx);
        }
        
        previous_errors = iterate_once(images_in_use, camera_matrix, distortion_coeffs);
    
        update_error_stats(images_in_use, previous_errors, camera_matrix);
        show_iteration_stats(images_in_use);
    } while (should_continue(images_in_use));
    
    _final_imageset = std::move(images_in_use);
    _camera_params.camera_matrix = camera_matrix;
    _camera_params.distortion_coefficients = distortion_coeffs;
    _camera_params.reprojection_error = _mean_errors.back();
    _camera_params.frame_count = _initial_imageset.size() - discarded_image_count;
    _camera_params.image_size = _initial_imageset.front().size();
    _camera_params.camera_name = _camera_name;
}


std::vector<double> dvrk::CalibrationDataset::iterate_once(
    std::vector<CalibrationImage>& images_in_use,
    cv::Mat& camera_matrix,
    cv::Mat& distortion_params
) {
    std::vector< std::vector<cv::Point3f> > all_object_points;
    std::vector< std::vector<cv::Point2f> > all_image_points;
    
    for (const auto& image : images_in_use) {
        all_object_points.push_back(image.getObjectPoints());
        all_image_points.push_back(image.getImagePoints());
    }
    
    assert(all_object_points.size() == all_image_points.size());
    assert(images_in_use.size() > 0);
    
    cv::Size image_size{ images_in_use.front().size() };
    std::vector<cv::Mat> rotation_vecs, translation_vecs;
    cv::calibrateCamera(
        all_object_points, all_image_points, image_size,                   // Inputs
        camera_matrix, distortion_params, rotation_vecs, translation_vecs, // Outputs
        _calibration_flags                                                 // Configuration
    );
    
    for (size_t i = 0; i < rotation_vecs.size(); ++i) {
        images_in_use.at(i).setPose(rotation_vecs[i], translation_vecs[i]);
    }
    
    std::vector<double> reprojection_errors;
    for (auto& image : images_in_use) {
        image.set_intrinsics(camera_matrix, distortion_params);
        _all_reprojection_errors[image.getName()].push_back(image.get_reprojection_error_mm());
    }
    
    return reprojection_errors;
}


void dvrk::CalibrationDataset::update_error_stats(
    const std::vector<CalibrationImage> &images_in_use,
    const std::vector<double> &errors,
    const cv::Mat& camera_matrix
) {
    accumulator_set<double, stats<tag::mean, tag::variance, tag::max>> acc;
    acc = std::for_each(errors.begin(), errors.end(), acc);
    
    _fx.push_back(camera_matrix.at<double>(0, 0));
    _fy.push_back(camera_matrix.at<double>(1, 1));
    _cx.push_back(camera_matrix.at<double>(0, 2));
    _cy.push_back(camera_matrix.at<double>(1, 2));
    
    _max_error.push_back(max(acc));
    _mean_errors.push_back(mean(acc));
    _std_deviations.push_back(sqrt(variance(acc)));
}


void dvrk::CalibrationDataset::show_iteration_stats(
    const std::vector<CalibrationImage>& images_in_use
) {
    const auto mean = _mean_errors.back();
    const auto std_dev = _std_deviations.back();
    const auto max_error = _max_error.back();
    
    const auto& image_size = images_in_use.front().size();
    const auto cx_ideal = image_size.width / 2;
    const auto cy_ideal = image_size.height / 2;
    
    double delta_mean = 0.0, delta_dev = 0.0, delta_max = 0.0;
    if (_mean_errors.size() >= 2) {
        delta_mean = std::abs(_mean_errors.end()[-1] - _mean_errors.end()[-2]);
        delta_dev = std::abs(_std_deviations.end()[-1] - _std_deviations.end()[-2]);
        delta_max = std::abs(_max_error.end()[-1] - _max_error.end()[-2]);
    }
    
    std::cout << std::setprecision(4);

    std::cout << "Iteration #" << _mean_errors.size() - 1 << " Using "
              << images_in_use.size() << " of " << _initial_imageset.size() << " images\n"
              << "  Stats - Mean: " << mean << "px, (Δ = " << delta_mean << "), "
              << "Std. Dev.: " << std_dev << "px, (Δ = " << delta_dev << "), "
              << "Max: " << max_error << "px, (Δ = " << delta_max << ")\n"
              << "  Intrinsics:\n"
              << "    Focal Lengths - Fx: " << _fx.back() << "px, Fy: " << _fy.back() << "px\n"
              << "    Image Centre  - Cx: " << _cx.back() << "px (Ideal: " << cx_ideal << "), "
              << "Cy: " << _cy.back() << "px (Ideal: " << cy_ideal << ")\n";
}


/**
 * Termination criteria:
 *  - The error is less than the specified threshold
 *  - The imageset is not too small
 */
bool dvrk::CalibrationDataset::should_continue(const std::vector<CalibrationImage>& images_in_use) {
    auto err_delta = std::numeric_limits<double>::max();
    if (_mean_errors.size() >= 2) {
        err_delta = std::abs(_mean_errors.end()[-1] - _mean_errors.end()[-2]);
    }
    
    bool is_monotonically_decreasing =
        ((*std::min_element(_mean_errors.begin(), _mean_errors.end())) == _mean_errors.back());
    
    return is_monotonically_decreasing
        && err_delta > _min_iteration_change
        && images_in_use.size() > _min_imageset_size;
}


void dvrk::CalibrationDataset::save_details(const fs::path &filename)
{
    std::ofstream outfile(filename.string(), std::ios::out);
    if (!outfile) {
        BOOST_THROW_EXCEPTION(file_open_error() << errfile(filename.string()));
    }
    
    outfile << "Iteration Number, Mean Error, Std. Dev., Max Error, Fx, Fy, Cx, Cy\n";
    for (size_t idx = 0; idx < _mean_errors.size(); ++idx) {
        outfile << idx << ", " << _mean_errors.at(idx)
                << ", " << _std_deviations.at(idx)
                << ", " << _max_error.at(idx)
                << ", " << _fx.at(idx) << ", " << _fy.at(idx)
                << ", " << _cx.at(idx) << ", " << _cy.at(idx)
                << "\n";
    }
    
    outfile.close();
}


void dvrk::CalibrationDataset::save_extra_details(const fs::path& filename) {
    std::ofstream csv(filename.string(), std::ios::out);
    if (!csv) {
        BOOST_THROW_EXCEPTION(file_open_error() << errmsg(filename.string()));
    }
    
    csv << "Image Name, Reprojection Error (mm)\n";
    for (const auto& kv: _all_reprojection_errors) {
        std::string image_name = kv.first;
        std::vector<double> errors = kv.second;
        
        csv << "\"" << image_name << "\"";
        for (const auto err: errors) csv << ", " << err;
        csv << "\n";
    }
    
    csv.close();
}


void dvrk::CalibrationDataset::save_calibration(const fs::path& filename) {
    _camera_params.to_file(filename);
}


void dvrk::CalibrationDataset::set_calibration_flags(int flags) {
    _calibration_flags = flags;
    
    if (_calibration_flags & cv::CALIB_RATIONAL_MODEL) {
        _camera_params.distortion_model = "rational_polynomial";
    } else {
        _camera_params.distortion_model = "plumb_bob";
    }
    
}
