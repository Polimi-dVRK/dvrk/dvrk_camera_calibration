//
// Created by tibo on 14/11/17.
//

#include <iomanip>
#include <chrono>

#include <boost/optional.hpp>
#include "dvrk_camera_calibration/error.hpp"
#include "dvrk_camera_calibration/camera_params.hpp"

using namespace std::string_literals;
using namespace dvrk;

template <typename T>
boost::optional<T> get_value(const cv::FileNode& node) {
    if (node.isNone()) return {};
    
    T retval;
    node >> retval;
    return retval;
}


CameraParams dvrk::CameraParams::FromFile(cv::FileStorage& file) {
    CameraParams params;
    
    try {
        if (const auto res = get_value<cv::Mat>(file["camera_matrix"])) {
            params.camera_matrix = *res;
        }
        else {
            BOOST_THROW_EXCEPTION(key_error()
                << errmsg("Missing key 'camera_matrix'")
                << errkey("camera_matrix")
            );
        }
        
        if (const auto res = get_value<std::string>(file["distortion_model"])) {
            params.distortion_model = *res;
            
            if (params.distortion_model != "rational_polynomial" &&
                params.distortion_model != "plumb_bob"
            ) {
                BOOST_THROW_EXCEPTION(out_of_range_error()
                    << errmsg("Invalid distortion model. The distortion model should be either 'plumb_bob' or 'rational_polynomial'")
                );
            }
        }
        else {
            BOOST_THROW_EXCEPTION(key_error()
                << errmsg("Missing key 'distortion_coefficients'")
                << errkey("distortion_coefficients")
            );
        }
        
        if (const auto res = get_value<cv::Mat>(file["distortion_coefficients"])) {
            params.distortion_coefficients = *res;
        }
        else {
            BOOST_THROW_EXCEPTION(key_error()
                << errmsg("Missing key 'distortion_coefficients'")
                << errkey("distortion_coefficients")
            );
        }
        
        if (const auto res = get_value<cv::Mat>(file["rectification_matrix"])) {
            params.rectification_matrix = *res;
        }
        else {
            BOOST_THROW_EXCEPTION(key_error()
                << errmsg("Missing key 'rectification_matrix'")
                << errkey("rectification_matrix")
            );
        }
        
        if (const auto res = get_value<cv::Mat>(file["projection_matrix"])) {
            params.projection_matrix = *res;
        }
        else {
            BOOST_THROW_EXCEPTION(key_error()
                << errmsg("Missing key 'projection_matrix'")
                << errkey("projection_matrix")
            );
        }
        
        if (const auto res = get_value<std::string>(file["calibration_date"])) {
            params.calibration_date = *res;
        }
        
        if (const auto res = get_value<std::string>(file["calibration_tool"])) {
            params.calibration_tool = *res;
        }
        
        if (const auto res = get_value<double>(file["reprojection_error"])) {
            params.reprojection_error = *res;
        }
        
        if (const auto res = get_value<int>(file["image_width"])) {
            params.image_size.width = *res;
        }
        
        if (const auto res = get_value<int>(file["image_height"])) {
            params.image_size.height = *res;
        }
        
        if (const auto res = get_value<std::string>(file["camera_name"])) {
            params.camera_name = *res;
        }
    
        if (const auto res = get_value<int>(file["frame_count"])) {
            params.frame_count = *res;
        }
        
        if (const auto res = get_value<int>(file["stereo_frame_count"])) {
            params.stereo_frame_count = *res;
        }
    }
    catch (cv::Exception& ex) {
        BOOST_THROW_EXCEPTION(file_parse_error() << errmsg(ex.err));
    }
    
    return params;
}


CameraParams dvrk::CameraParams::FromFile(const fs::path& filename) {
    if (!fs::exists(filename) && fs::is_regular_file(filename)) {
        BOOST_THROW_EXCEPTION(not_a_file_error()
            << errfile(filename.string())
            << errmsg(filename.string() + "is not a file.")
        );
    }
    
    cv::FileStorage file(filename.string(), cv::FileStorage::READ);
    if (!file.isOpened()) {
        BOOST_THROW_EXCEPTION(file_open_error()
            << errfile(filename.string())
            << errmsg("Unable to open file "s + filename.string())
        );
    }
    
    CameraParams params;
    try {
        params = FromFile(file);
    } catch (runtime_error &ex) {
        ex << errfile(filename.string());
        throw;
    }
    
    file.release();
    return params;
}


void dvrk::CameraParams::to_file(cv::FileStorage& file) {
    
    std::time_t now = std::chrono::system_clock::to_time_t(
        std::chrono::system_clock::now()
    );
    
    std::ostringstream local_time;
    local_time << std::put_time(std::localtime(&now), "%F %T");
    std::string local_time_str = local_time.str();
    
    file << "calibration_date" << local_time_str;
    file << "calibration_tool" << "dvrk_camera_calibration";
    file << "reprojection_error" << reprojection_error;
    file << "image_width" << image_size.width;
    file << "image_height" << image_size.height;
    file << "camera_name" << camera_name;
    file << "frame_count" << frame_count;
    file << "stereo_frame_count" << stereo_frame_count;
    file << "camera_matrix" << camera_matrix;
    file << "distortion_model" << distortion_model;
    file << "distortion_coefficients" << distortion_coefficients;
    file << "rectification_matrix" << rectification_matrix;
    file << "projection_matrix" << projection_matrix;
}


void dvrk::CameraParams::to_file(const fs::path& filename) {
    cv::FileStorage outfile(filename.string(), cv::FileStorage::WRITE);
    if (!outfile.isOpened()) {
        BOOST_THROW_EXCEPTION(file_open_error() << errfile(filename.string()));
    }
    
    to_file(outfile);
    outfile.release();
}

