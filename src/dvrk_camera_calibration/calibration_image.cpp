//
// Created by tibo on 23/10/17.
//
//
// Created by tibo on 20/08/17.
//

#include <numeric>

#include <boost/numeric/conversion/cast.hpp>

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

#include "dvrk_camera_calibration/util.hpp"
#include "dvrk_camera_calibration/draw.hpp"
#include "dvrk_camera_calibration/error.hpp"
#include "dvrk_camera_calibration/calibration_pattern.hpp"
#include "dvrk_camera_calibration/calibration_image.hpp"

using namespace dvrk;
using namespace std::string_literals;

/*
 *  Public Methods - Constructors
 */

CalibrationImage::CalibrationImage(
        const cv::Mat& image,
        const CalibrationPattern& pattern,
        const std::string& name
    )
{
    initialise(image, pattern, defaultObjectPoints(pattern), name);
}


CalibrationImage::CalibrationImage(
        const cv::Mat& image,
        const CalibrationPattern& pattern,
        const std::vector<cv::Point3f>& object_points,
        const std::string& name
    ) {
    initialise(image, pattern, object_points, name);
}


CalibrationImage::CalibrationImage(
        const std::string& filename,
        const CalibrationPattern& pattern,
        const std::string& name
    ) {
    initialise(read_image(filename), pattern, defaultObjectPoints(pattern), name);
}


CalibrationImage::CalibrationImage(const std::string &filename,
        const CalibrationPattern &pattern,
        const std::vector<cv::Point3f> &object_points)
{
    initialise(read_image(filename), pattern, object_points, filename);
}


CalibrationImage::CalibrationImage(
    const fs::path &filename,
    const CalibrationPattern &pattern
) {
    initialise(read_image(filename), pattern, defaultObjectPoints(pattern), filename.string());
}


CalibrationImage::CalibrationImage(
    const fs::path &filename,
    const CalibrationPattern &pattern,
    const std::vector<cv::Point3f> &object_points
) {
    initialise(read_image(filename), pattern, object_points, filename.string());
}

/*
 * Protected Methods - Initialisation
 */

void CalibrationImage::initialise(
    const cv::Mat& image,
    const CalibrationPattern& pattern,
    const std::vector<cv::Point3f>& object_points,
    const std::string& name
) {
    if (image.empty())
        BOOST_THROW_EXCEPTION(empty_image_error() << errmsg("Image should not be empty"));
    
    const auto num_corners = pattern.size.width * pattern.size.height;
    if (object_points.size() != num_corners) {
        BOOST_THROW_EXCEPTION(size_error()
            << errmsg("There should be as many object points as corners. Found "s + std::to_string(_object_points.size()) + " corners in " + _name));
    }
    
    _image = image.clone();
    _pattern = pattern;
    _object_points = object_points;
    _name = name;
    
    locate_chessboard();
}


std::vector<cv::Point3f> CalibrationImage::defaultObjectPoints(const CalibrationPattern& pattern) {
    std::vector<cv::Point3f> corners;
    
    for (auto i = 0; i < pattern.size.height; i++) {
        for (auto j = 0; j < pattern.size.width; j++) {
            corners.emplace_back(j * pattern.feature_size, i * pattern.feature_size, 0.0f);
        }
    }
    
    return corners;
}


/*
 * Protected Methods - Processing
 */

void CalibrationImage::locate_chessboard() {
    _chessboard_found = cv::findChessboardCorners(_image, _pattern.size, _image_points);
    
    if (!_chessboard_found)
        return;
    
    if (_image_points.size() != _object_points.size()) {
        BOOST_THROW_EXCEPTION(runtime_error()
            << errmsg("`image_points` and `object_points` should have the same size"));
    }
    
    // We need to compute a search radius for the subpixel optimisation step. It seems that
    // using a radius of half the minimum distance between corners is large enough to snap to
    // the correct corner without including the wrong corners in the search window.
    double min_distance = std::numeric_limits<double>::max();
    for (const cv::Point2f& first : _image_points) {
        for (const cv::Point2f& second : _image_points) {
            if (first != second) {
                const cv::Vec2f radius = (first - second) / 2.0;
                min_distance = std::min(min_distance, cv::norm(radius, cv::NORM_L2));
            }
        }
    }
    
    cv::Mat grayscale_img;
    cv::cvtColor(_image, grayscale_img, cv::COLOR_RGB2GRAY, 1);
    
    auto search_radius = boost::numeric_cast<int>(min_distance);
    cv::cornerSubPix(
        grayscale_img, _image_points, cv::Size(search_radius, search_radius), cv::Size(-1, -1),
        cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1)
    );
    
    // Having extracted the corners we can compute all the metrics we need
    _outside_corners = computeBoardOuterCorners(_image_points, _pattern);
    _board_area = computeBoardArea();
    _board_skew = computeBoardSkew();
}


CalibrationImage::OutsideCorners CalibrationImage::computeBoardOuterCorners(
    const std::vector<cv::Point2f>& image_points,
    const CalibrationPattern& pattern
) {
    OutsideCorners corners;
    
    corners.top_left = _image_points.at(0);
    corners.top_right = _image_points.at(static_cast<size_t>(pattern.size.width));
    
    auto bottom_left_idx = pattern.size.width * (pattern.size.height - 1);
    corners.bottom_left = _image_points.at(static_cast<size_t>(bottom_left_idx));
    corners.bottom_right =
        _image_points.at(static_cast<size_t>(bottom_left_idx + pattern.size.width - 1));
    
    return corners;
}


double CalibrationImage::computeBoardSkew()
{
    // If we are viewwing the target head on then the angle of each corner of the chessboard is 90°. As the board moves
    // this angle changes. By computing the angle of one corner of the board we can estimate the skew.
    
    const auto a = _outside_corners.top_left;
    const auto b = _outside_corners.top_right;
    const auto c = _outside_corners.bottom_right;
    
    cv::Vec2f ab = a - b, cb = c - b;
    cv::normalize(ab, ab);
    cv::normalize(cb, cb);
    const auto angle = std::acos(ab.dot(cb)); // Angle between the vectors ab and cb
    
    const double skew = 2.0 * std::abs(M_PI_2 - angle);
    return std::min(1.0, skew);
}


double CalibrationImage::computeBoardArea()
{
    /*
     * We assume that the projection of the chessboard in the image is a regular quadrilateral
     * in a single plane. The quadrilateral has 4 sides labelled a, b, c and d and two diagonals
     * labelled p and q where the following hold true:
     *
     *  - a + b + c + d = 0
     *  - p = b + c & q = a + b
     *
     *  The area of such a quadrilateral is:
     *
     *  A = 1/2 * det(p * q) = 1/2 ( px*qy - py*qx )
     *
     *  See http://mathworld.wolfram.com/Quadrilateral.html for more information.
     */
    
    const auto a = _outside_corners.top_right - _outside_corners.top_left;
    const auto b = _outside_corners.bottom_right - _outside_corners.top_right;
    const auto c = _outside_corners.bottom_left - _outside_corners.bottom_right;
    
    const auto p = b + c;
    const auto q = a + b;
    
    return std::abs(p.x * q.y - p.y * q.x) / 2.0;
}


cv::Point2f CalibrationImage::computeBoardCentroid() {
    const cv::Point2f sum_of_points = std::accumulate(
        _image_points.begin(), _image_points.end(), cv::Point2f(0, 0), [](auto sum, auto el) {
            return cv::Point2f(sum.x + el.x, sum.y + el.y);
        }
    );
    return sum_of_points / boost::numeric_cast<int>(_image_points.size());
}

/*
 * Non-Trivial Getters & Setters
 */

void CalibrationImage::setPose(cv::Mat& rotation, cv::Mat& translation) {
    _pose.rotation = rotation;
    _pose.translation = translation;
}


std::vector<cv::Point2f> CalibrationImage::reproject() const {
    if (_camera_matrix.empty() || _distortion_coeffs.empty()) {
        BOOST_THROW_EXCEPTION(runtime_error()
            << errmsg("Load the camera intrinsics before attempting to reproject"));
    }
    
    _reprojected_image_points.resize(_object_points.size());
    cv::projectPoints(
        _object_points, _pose.rotation, _pose.translation, _camera_matrix, _distortion_coeffs, // Inputs
        _reprojected_image_points                                                              // Outputs
    );
    
    return _reprojected_image_points;
}


void
CalibrationImage::compute_pose() {
    cv::solvePnP(
        _object_points, _image_points, _camera_matrix, _distortion_coeffs, // Inputs
        _pose.rotation, _pose.translation                                  // Outputs
    );
}

double CalibrationImage::get_reprojection_error() const {
    if (_reprojected_image_points.empty()) {
        reproject();
    }
    
    const auto err = cv::norm(_image_points, _reprojected_image_points, cv::NORM_L2);
    const auto rms = std::sqrt(err * err / _image_points.size());
    
    return rms;
}


double CalibrationImage::get_reprojection_error_mm() const {
    if (_reprojected_image_points.empty()) {
        reproject();
    }
    
    const auto err = cv::norm(_image_points, _reprojected_image_points, cv::NORM_L2);
    
    // We need to  convert the error in px to an error in mm.
    // First we need to convert from px to mm. For this we're going to  measure the size of one of
    // the centre squares in px and pretend that all of the other squares have the same size. This
    // obviously not true but is a decent first order approximation.

    const auto centre_corner_idx =
        _pattern.size.width * (_pattern.size.height / 2) + _pattern.size.width / 2;
    assert(_image_points.size() > centre_corner_idx + 1);
    
    cv::Point2f first_point = _image_points[centre_corner_idx];
    cv::Point2f next_point = _image_points[centre_corner_idx + 1];
    const double square_size = cv::norm(first_point - next_point);
    
    // Once we have the square size we just need to calculate the conversion factor from mm to
    // pixels and apply it.
    // Note: since feature_size is in metres we need to convert it to mm first.
    const double mm_per_px = (_pattern.feature_size * 1000) / square_size;
    const double err_mm = err * mm_per_px;
    
    const auto rms = std::sqrt(err_mm * err_mm / _image_points.size());
    return rms;
}

void CalibrationImage::set_intrinsics(const cv::Mat& camera_matrix, const cv::Mat& distortion_coeffs) {
    _camera_matrix = camera_matrix;
    _distortion_coeffs = distortion_coeffs;
    
    _reprojected_image_points.clear();
    compute_pose();
}


cv::Mat CalibrationImage::show_points(bool show_detected, bool show_reprojected) {
    cv::Mat canvas = _image.clone();
    if (show_detected) {
        for (const auto corner : _image_points) {
            draw_cross(canvas, corner, /* marker_size = */ 10, cv::Scalar(0, 254, 124));
        }
    }
    
    if (show_reprojected) {
        reproject();
        for (const auto corner : _reprojected_image_points) {
            draw_cross(canvas, corner, /* marker_size = */ 10, cv::Scalar(0, 0, 255));
        }
    }
    
    return canvas;
}
