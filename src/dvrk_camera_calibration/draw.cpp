//
// Created by tibo on 22/12/17.
//

#include <iomanip>
#include <opencv2/imgproc.hpp>

#include "dvrk_camera_calibration/draw.hpp"

using namespace dvrk;

void dvrk::draw_cross(cv::Mat& image, cv::Point position, int size, cv::Scalar colour) {
    cv::line(
        image,
        cv::Point( position.x - size / 2, position.y ),
        cv::Point( position.x + size / 2, position.y ),
        colour
    );
    cv::line(
        image,
        cv::Point( position.x, position.y + size / 2),
        cv::Point( position.x, position.y - size / 2),
        colour
    );
}

void dvrk::draw_errors(
    cv::Mat& canvas,
    const std::vector<cv::Point2f>& detected_image_points,
    const std::vector<cv::Point2f>& reprojected_image_points,
    const float error_scale,
    const ErrorRepresentation error_representation,
    const int marker_size
) {
    cv::Scalar LawnGreen(0, 252, 124), Red(0,   0, 255), Purple(128,   0, 128);
    
    if (error_representation == ErrorRepresentation::Crosses) {
        for (const auto& point : detected_image_points) {
            draw_cross(canvas, point, marker_size, LawnGreen);
        }
        
        for (size_t i = 0; i < reprojected_image_points.size(); ++i) {
            cv::Point2f new_point =
                detected_image_points.at(i) + (detected_image_points.at(i) - reprojected_image_points.at(i)) * error_scale;
            
            draw_cross(canvas, new_point, marker_size, Red);
        }
        
        cv::Size imsz = canvas.size();
        draw_cross(canvas, cv::Point(30, imsz.height - 35), marker_size, LawnGreen);
        cv::putText(canvas, "Detected", cv::Point(40, imsz.height - 30), cv::FONT_HERSHEY_DUPLEX, .5, Purple);
        
        draw_cross(canvas, cv::Point(30, imsz.height - 65), marker_size, Red);
        cv::putText(canvas, "Reprojected", cv::Point(40, imsz.height - 60), cv::FONT_HERSHEY_DUPLEX, .5, Purple);
    } else if (error_representation == ErrorRepresentation::Lines) {
        for (size_t i = 0; i < detected_image_points.size(); ++i) {
            
            cv::Point2f errdir = (reprojected_image_points.at(i) - detected_image_points.at(i)) * error_scale;
            
            cv::line(
                canvas,
                detected_image_points.at(i),
                detected_image_points.at(i) + errdir,
                LawnGreen,
                1,
                cv::LINE_AA
            );
        }
    }
    
    std::ostringstream msg;
    msg << "Error Scale: " << std::setprecision(3) << error_scale;
    cv::putText(canvas, msg.str(), {10, 10}, cv::FONT_HERSHEY_DUPLEX, .5, Purple);
}
