//
// Created by tibo on 23/10/17.
//

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/persistence.hpp>

#include "dvrk_camera_calibration/util.hpp"
#include "dvrk_camera_calibration/error.hpp"

using namespace dvrk;

bool dvrk::is_image_file(const fs::path& file) {
    if (!fs::is_regular_file(file))
        return false;
    
    return (supported_image_formats.count(file.extension().string()) != 0);
}


cv::Mat dvrk::read_image(const boost::filesystem::path &filename) {
    if (!is_image_file(filename))
        BOOST_THROW_EXCEPTION(not_a_file_error()
            << errfile(filename.string())
            << errmsg(filename.string() + " is not an image file.")
        );
    
    cv::Mat image = cv::imread(filename.string());
    if (image.empty())
        BOOST_THROW_EXCEPTION(file_read_error()
            << errfile(filename.string())
            << errmsg("Unable to read " + filename.string() + ", image is empty"));
    
    return image;
}


cv::FileStorage dvrk::load_intrinsics(
    boost::filesystem::path intrinsics_file,
    cv::Mat& camera_matrix,
    cv::Mat& distortion_coefficients
) {
    if (!fs::is_regular_file(intrinsics_file)) {
        BOOST_THROW_EXCEPTION(not_a_file_error() << errfile(intrinsics_file.string()));
    }
    
    cv::FileStorage infile(intrinsics_file.string(), cv::FileStorage::READ);
    if (!infile.isOpened()) {
        BOOST_THROW_EXCEPTION(file_read_error() << errfile(intrinsics_file.string()));
    }
    
    infile["camera_matrix"] >> camera_matrix;
    infile["distortion_coefficients"] >> distortion_coefficients;
    
    return infile;
}

