//
// Created by tibo on 09/11/17.
//

#include <iostream>
#include <dvrk_camera_calibration/calibration_dataset.hpp>

#include "common.hpp"

float compute_average_difference(const cv::Mat& lhs, const cv::Mat& rhs) {
    // We can only compare matrices of the same type.
    assert(lhs.type() == rhs.type());
    
    cv::Mat diff;
    cv::absdiff(lhs, rhs, diff);
    cv::Scalar img_mean = cv::mean(diff);
    
    // The cv::mean function returns a cv::Scalar with 4 components always. We only want the ones
    // that correspond to actual image channels
    float mean_diff = 0.f;
    for (int i = 0; i < lhs.channels(); ++i) {
        mean_diff += img_mean[i];
    }
    
    return mean_diff / lhs.channels();
}

dvrk::CalibrationDataset load_images(
    const cv::FileNode& imagelist,
    const dvrk::CalibrationPattern& pattern,
    const fs::path& base_path
) {
    if (imagelist.isNone()) {
        std::cerr << "Error: Could not locate 'images' node. Unable to continue." << std::endl;
        exit(-1);
    }
    
    if (!imagelist.isSeq()) {
        std::cerr << "Error: 'images' node in the configuration file should be a list of file names." << std::endl;
        exit(-1);
    }
    
    dvrk::CalibrationDataset imageset;
    size_t non_existent_files = 0;
    size_t images_with_no_calibration_pattern = 0;
    for (const auto &file: imagelist) {
        try {
            dvrk::CalibrationImage
                new_image(base_path / file.string().c_str(), pattern);
            if (new_image.hasChessboard()) {
                imageset.add(new_image);
                std::cout << "  - Added " << new_image.getName() << " with "
                          << new_image.getImagePoints().size() << " image points\n";
            }
            else {
                std::cout << "  - Ignored " << new_image.getName() << ", chessboard not found\n";
                images_with_no_calibration_pattern += 1;
            }
        }
        catch (const dvrk::not_a_file_error& ex) {
            std::cout << "  - Warning: Unable to open " << file.string() << std::endl;
            non_existent_files += 1;
        }
    }
    
    std::cout << "\nImage initialisation complete.\n"
              << "  - Processed " << imagelist.size() << " candidate images\n"
              << "  - Retained " << imageset.size() << " images with readable calibration patterns\n";
    if (non_existent_files > 0) {
        std::cout << "  - Discarded " << non_existent_files << " images that could not be opened\n";
    }
    if (images_with_no_calibration_pattern > 0) {
        std::cout << "  - Discarded " << images_with_no_calibration_pattern << " images in which the calibration pattern could not be read.\n";
    }
    
    return imageset;
}
