//
// Created by tibo on 08/01/18.
//

#include <mutex>
#include <string>
using namespace std::string_literals;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/util.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>

#include <dvrk_camera_calibration/geometry.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>

#include "common.hpp"

class PatternDetector {

public: /* Methods */
    PatternDetector(
        const dvrk::CalibrationPattern pattern,
        const std::string& image_name_prefix,
        const std::string& camera_name
    ) : _nh(),
        _camera(_nh),
        _image_name_prefix(image_name_prefix),
        _camera_name(camera_name),
        _pattern(pattern)
    {
        _camera.subscribe("image_raw", &PatternDetector::on_image, this);
        ROS_INFO_STREAM(
            "" << "Ready, waiting for images ... \n"
               << "    Inputs: " << _camera.getTopic() << ", " << _camera.getInfoTopic()
        );
    }


public: /* Callbacks */
    
    void on_image(
        const sensor_msgs::ImageConstPtr& image_ros,
        const image_geometry::PinholeCameraModel&
    ) {
        const auto image_cv = cv_bridge::toCvCopy(image_ros, sensor_msgs::image_encodings::BGR8);
        dvrk::CalibrationImage image(image_cv->image, _pattern);
        
        cv::Mat canvas = image.getImage();
        
        if (image.hasChessboard()) {
            const auto& image_points = image.getImagePoints();
            
            // We measure the rectification error as the distance between the reprojected points and
            // the line joining the first and last point in each line
            std::vector<float> rectification_errors;
            
            for (unsigned int row = 0; row < _pattern.size.height; ++row) {
                Line pattern_line(
                    image_points.at( _pattern.get_corner_idx(row, 0) ),
                    image_points.at( _pattern.get_corner_idx(row, _pattern.size.width - 1) )
                );
                cv::line(canvas, pattern_line.p1, pattern_line.p2, cv::Scalar(0, 0, 255), cv::LINE_AA);
                
                for (unsigned int col = 0; col < _pattern.size.width; ++col) {
                    const auto& this_corner = image_points.at( _pattern.get_corner_idx(row, col) );
                    
                    if (col == 0 or col == (_pattern.size.width - 1)) {
                        cv::circle(canvas, this_corner, 5, cv::Scalar(0, 255, 0));
                    } else {
                        dvrk::draw_cross(canvas, this_corner, 5, cv::Scalar(0, 255, 0));
                    }
                    
                    if (col >= 1) {
                        const auto& prev_corner = image_points.at(_pattern.get_corner_idx(row, col-1));
                        cv::line(canvas, prev_corner, this_corner, cv::Scalar(255, 0 , 0), cv::LINE_AA);
                    }

                    rectification_errors.push_back(distance(pattern_line, this_corner));
                }
            }
            
            auto rectification_err = 0.f;
            for (const auto err : rectification_errors) { rectification_err += err; }
            rectification_err /= rectification_errors.size();
            
            std::string message{ "Rectification Error: " + std::to_string(rectification_err) };
            cv::putText(canvas, message, {30, 30}, cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 255, 0), .75, cv::LINE_AA);
        }
        
        if (!_is_window_created) {
            cv::namedWindow(_window_name, cv::WINDOW_NORMAL);
            _is_window_created = true;
        }
        
        cv::imshow(_window_name, image_cv->image);
        
        const auto key = dvrk::waitKey(1);
        switch(key) {
            case dvrk::KeyCode::Escape:
                ros::shutdown();
                break;
            
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(_window_name);
                break;
                
            
            default: break;
        }
    }

private: /* Members */
    
    const std::string _window_name{ "Test Window" };
    const std::string _image_name_prefix;
    const std::string _camera_name;
    
    ros::NodeHandle _nh;
    dvrk::SimpleCamera _camera;
    
    /* State Variables */
    bool _is_window_created = false;
    
    std::string _last_message{ "Press 's' to save image" };
    
    dvrk::CalibrationPattern _pattern;
};


int main(int argc, char** argv) {
    
    ros::init(argc, argv, "imview", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    
    float noise_threshold;
    unsigned int pattern_width, pattern_height;
    std::string image_name_prefix, camera_name, pattern_type;
    
    po::options_description main_opts("Options");
    main_opts.add_options()
                 ("help", "Show this help.")
                 (
                     "name-prefix,n",
                     po::value<std::string>(&image_name_prefix)->default_value("image"),
                     "The prefix used to name saved images"
                 ) (
        "camera-name,c",
        po::value<std::string>(&camera_name)->default_value("stereo-camera"),
        "The name of the camera, used to name the folder in which images are saved"
    ) (
        "pattern,p",
        po::value<std::string>(&pattern_type)->default_value("chessboard"),
        "The type of pattern to detect"
    ) (
        "width,w",
        po::value<unsigned int>(&pattern_width)->required(),
        "The width of the pattern"
    ) (
        "height,h",
        po::value<unsigned int>(&pattern_height)->required(),
        "The height of the pattern"
    ) (
        "noise-threshold,t",
        po::value<float>(&noise_threshold)->default_value(-1.f),
        "The maximum allowed mean difference allowed when saving images"
    );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << "Summary:\n"
                      << "    View an image topic in real time. \n"
                      << "\n"
                      << "Usage:\n"
                      << "    rosrun dvrk_common imview image:=/my_image_topic\n"
                      << "\n"
                      << "    The 'image' topic must be remapped to the topic you want to observe."
                      << "\n"
                      << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    cv::Size pattern_size { static_cast<int>(pattern_height), static_cast<int>(pattern_width) };
    
    // Note: We create a calibration pattern with a dummy feature size. It is only relevant when
    // estimating the camera pose.
    dvrk::CalibrationPattern pattern("chessboard", pattern_size, 1.0);
    PatternDetector detector(pattern, image_name_prefix, camera_name);
    
    ROS_INFO("Detector Ready ...");
    ros::spin();
    
    return 0;
}
