//
// Created by nearlab on 25/10/17.
//


#include <string>
#include <mutex>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/util.hpp>
#include <dvrk_common/ros/camera/stereo_camera.hpp>

#include <dvrk_camera_calibration/calibration_image.hpp>

#include "common.hpp"

using namespace std::string_literals;

class PatternDetector {

public: /* Methods */
    PatternDetector(
        const dvrk::CalibrationPattern pattern,
        const std::string& image_name_prefix,
        const std::string& camera_name
    ) : _nh(),
        _camera(_nh),
        _image_name_prefix(image_name_prefix),
        _camera_name(camera_name),
        _pattern(pattern)
    {
        _camera.subscribe("left/image_raw", "right/image_raw", &PatternDetector::on_image, this);
        ROS_INFO_STREAM(
            "" << "Ready, waiting for images ... \n"
               << "    Inputs: " << _camera.getLeftImageTopic() << ", " << _camera.getLeftInfoTopic() << "\n"
               << "            " << _camera.getRightImageTopic() << ", " << _camera.getRightInfoTopic() << "\n"
        );
    }
    


public: /* Callbacks */
    
    void on_image(
        const sensor_msgs::ImageConstPtr& left_image,
        const sensor_msgs::ImageConstPtr& right_image,
        const image_geometry::StereoCameraModel&
    ) {
        ROS_ASSERT(left_image->width == right_image->width
                && left_image->height == right_image->height);
        
        if (!_is_window_created) {
            cv::namedWindow(_window_name, cv::WINDOW_NORMAL);
            _is_window_created = true;
        }
        
        const auto left_image_cv = cv_bridge::toCvCopy(left_image, sensor_msgs::image_encodings::BGR8);
        const auto right_image_cv = cv_bridge::toCvCopy(right_image, sensor_msgs::image_encodings::BGR8);
        
        {
            std::lock_guard<std::mutex> image_lock(_current_image_guard);
            _previous_left_image = _current_left_image;
            _current_left_image = dvrk::CalibrationImage(left_image_cv->image, _pattern);
            
            _previous_right_image = _current_right_image;
            _current_right_image = dvrk::CalibrationImage(right_image_cv->image, _pattern);
        }
    
        cv::Mat canvas(left_image->height, left_image->width * 2, left_image_cv->image.type(), dvrk::Colour::White);
        cv::Mat left_viewport = canvas(
            cv::Rect(cv::Point(0, 0), cv::Size(left_image->width, left_image->height))
        );
        left_image_cv->image.copyTo(left_viewport);
        
        cv::Mat right_viewport = canvas(
            cv::Rect(cv::Point(left_image->width, 0), cv::Size(left_image->width, right_image->height))
        );
        right_image_cv->image.copyTo(right_viewport);
        
        cv::drawChessboardCorners(
            left_viewport, _pattern.size, _current_left_image.getImagePoints(), _current_left_image.hasChessboard());
        cv::drawChessboardCorners(
            right_viewport, _pattern.size, _current_right_image.getImagePoints(), _current_right_image.hasChessboard());
        
        cv::Scalar text_colour = _last_message_is_error? dvrk::Colour::OrangeRed : dvrk::Colour::Purple;
        cv::putText(canvas, _last_message_first_line, {30, 30}, cv::FONT_HERSHEY_DUPLEX, 0.5, text_colour, .75, cv::LINE_AA);
        cv::putText(canvas, _last_message_second_line, {30, 60}, cv::FONT_HERSHEY_DUPLEX, 0.5, text_colour, .75, cv::LINE_AA);
    
        if (_flash_screen_green) {
            canvas.setTo(dvrk::Colour::White);
            _flash_screen_green = false;
        }
        
        cv::imshow(_window_name, canvas);
        
        const auto key = dvrk::waitKey(1);
        switch(key) {
            case dvrk::KeyCode::Escape:
                ros::shutdown();
                break;
            
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(_window_name);
                break;
            
            case dvrk::KeyCode::S:
                save_current_images();
                break;
            
            default: break;
        }
    }
    
    void save_current_images() {
        // Make sure to lock early so that the image doesn't change.
        // If there are many images in the folder already it might take a while to find a free name
        // and if the image is updated in the meantime we won't be saving the right image.
        std::lock_guard<std::mutex> image_lock(_current_image_guard);
    
        // We won't take the picture if the image is not stable. We just check if the previous
        // image is close enough to the current image.
        float left_image_change = compute_average_difference(
            _previous_left_image.getImage(), _current_left_image.getImage());
        float right_image_change = compute_average_difference(
            _previous_right_image.getImage(), _current_right_image.getImage());
        float max_image_change = std::max(left_image_change, right_image_change);
        
        if (_noise_threshold > 0 && max_image_change > _noise_threshold) {
            _last_message_first_line = "Noise threshold exceeded (" + std::to_string(max_image_change) + " > " + std::to_string(_noise_threshold) + ")";
            _last_message_is_error = true;
            return;
        }
        
        if (!_current_right_image.hasChessboard() or !_current_left_image.hasChessboard()) {
            _last_message_first_line = "Cannot save image pair where the chessboard is not present";
            _last_message_second_line = "";
            _last_message_is_error = true;
            ROS_WARN_STREAM(_last_message_first_line.c_str());
            return;
        }
        
        const auto ros_home = dvrk::getROSHome();
        const auto left_image_dir = ros_home / _camera_name / "left";
        const auto right_image_dir = ros_home / _camera_name / "right";
    
        // Make sure that the directories for the images exist:
        if (fs::create_directories(left_image_dir)) {
            ROS_INFO_STREAM("Created directory " << left_image_dir);
        }
        
        if (fs::create_directories(right_image_dir)) {
            ROS_INFO_STREAM("Created directory " << right_image_dir);
        }
        
        // try to find a name that isn't already in use for the new image files.
        for (static int image_number = 0; image_number < 1000; ++image_number) {
            const auto image_name{ _image_name_prefix + "_" + std::to_string(image_number) + ".png" };
            const auto left_image_name = left_image_dir / image_name;
            const auto right_image_name = right_image_dir / image_name;
            
            if (!fs::exists(left_image_name) and !fs::exists(right_image_name)) {
                if (!cv::imwrite(left_image_name.string(), _current_left_image.getImage())) {
                    std::cerr << "Error: Unable to write image to " << left_image_name << std::endl;
                    exit(-1);
                }
    
                if (!cv::imwrite(right_image_name.string(), _current_right_image.getImage())) {
                    std::cerr << "Error: Unable to write image to " << right_image_name << std::endl;
                    exit(-1);
                }
    
                _last_message_first_line = "Saved left image to "s + left_image_name.string();
                _last_message_second_line = "Saved right image to"s + right_image_name.string();
                _last_message_is_error = false;
                ROS_INFO_STREAM(_last_message_first_line << "\n" << _last_message_second_line << "\n Noise level was: " << max_image_change);
                
                _flash_screen_green = true;
                return;
            }
        }
        
        
        std::cerr << "Error: Unable to find a name for the latest image pair." << std::endl;
        exit(-1);
    }
    
    void set_noise_threshold(const float threshold) { _noise_threshold = threshold; }

private: /* Members */
    
    const std::string _window_name{ "Stereo View Window" };
    const std::string _image_name_prefix;
    const std::string _camera_name;
    
    std::string _last_message_first_line{ "Press 's' to save image" };
    std::string _last_message_second_line;
    bool _last_message_is_error = false;
    
    ros::NodeHandle _nh;
    dvrk::StereoCamera _camera;
    
    float _noise_threshold = -1.0f;
    
    /* State Variables */
    bool _is_window_created = false;
    bool _flash_screen_green = false;
    
    std::mutex _current_image_guard;
    
    dvrk::CalibrationImage _current_left_image;
    dvrk::CalibrationImage _current_right_image;
    
    dvrk::CalibrationImage _previous_left_image;
    dvrk::CalibrationImage _previous_right_image;
    
    dvrk::CalibrationPattern _pattern;
};


int main(int argc, char** argv) {
    
    ros::init(argc, argv, "imview", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    
    float noise_threshold;
    unsigned int pattern_width, pattern_height;
    std::string image_name_prefix, camera_name, pattern_type;
    
    po::options_description main_opts("Options");
    main_opts.add_options()
         ("help", "Show this help.")
         (
             "name-prefix,n",
             po::value<std::string>(&image_name_prefix)->default_value("image"),
             "The prefix used to name saved images"
         ) (
             "camera-name,c",
             po::value<std::string>(&camera_name)->default_value("stereo-camera"),
             "The name of the camera, used to name the folder in which images are saved"
         ) (
            "pattern,p",
            po::value<std::string>(&pattern_type)->default_value("chessboard"),
            "The type of pattern to detect"
        ) (
            "width,w",
            po::value<unsigned int>(&pattern_width)->required(),
            "The width of the pattern"
        ) (
            "height,h",
            po::value<unsigned int>(&pattern_height)->required(),
            "The height of the pattern"
        ) (
            "noise-threshold,t",
            po::value<float>(&noise_threshold)->default_value(-1.f),
            "The maximum allowed mean difference allowed when saving images"
        );
    
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << "Summary:\n"
                      << "    View an image topic in real time. \n"
                      << "\n"
                      << "Usage:\n"
                      << "    rosrun dvrk_common imview left_image:=/my_camera/left/image_rect right_image:=/my_camera/right/image_rect\n"
                      << "\n"
                      << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    cv::Size pattern_size { static_cast<int>(pattern_height), static_cast<int>(pattern_width) };
    dvrk::CalibrationPattern pattern("chessboard", pattern_size, 1.0);
    
    PatternDetector detector(pattern, image_name_prefix, camera_name);
    detector.set_noise_threshold(noise_threshold);
    ROS_INFO("Detector Ready ...");

    ros::spin();
    
    return 0;
}
