//
// Created by nearlab on 25/10/17.
//


#include <mutex>
#include <string>
using namespace std::string_literals;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/util.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>

#include <dvrk_camera_calibration/calibration_image.hpp>

#include "common.hpp"

class PatternDetector {

public: /* Methods */
    PatternDetector(
        const dvrk::CalibrationPattern pattern,
        const std::string& image_name_prefix,
        const std::string& camera_name
    ) : _nh(),
        _camera(_nh),
        _image_name_prefix(image_name_prefix),
        _camera_name(camera_name),
        _pattern(pattern)
    {
        _camera.subscribe("image_raw", &PatternDetector::on_image, this);
        ROS_INFO_STREAM(
            "" << "Ready, waiting for images ... \n"
               << "    Inputs: " << _camera.getTopic() << ", " << _camera.getInfoTopic()
        );
    }
    

public: /* Callbacks */

    void on_image(
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel&
    ) {
        const auto image_cv = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
    
        if (!_is_window_created) {
            cv::namedWindow(_window_name, cv::WINDOW_NORMAL);
            _is_window_created = true;
        }
        
        {
            std::lock_guard<std::mutex> image_lock(_current_image_guard);
            _previous_image = _current_image;
            _current_image = dvrk::CalibrationImage(image_cv->image, _pattern);
        }
        
        cv::drawChessboardCorners(
            image_cv->image, _pattern.size, _current_image.getImagePoints(), _current_image.hasChessboard());
            
        cv::Scalar text_colour = _last_message_is_error? dvrk::Colour::OrangeRed : dvrk::Colour::MediumPurple;
        cv::putText(image_cv->image, _last_message, {30, 30}, cv::FONT_HERSHEY_DUPLEX, 0.5, text_colour, .75, cv::LINE_AA);
    
        if (_flash_screen_green) {
            image_cv->image.setTo(dvrk::Colour::Green);
            _flash_screen_green = false;
        }
        
        cv::imshow(_window_name, image_cv->image);
    
        const auto key = dvrk::waitKey(1);
        switch(key) {
            case dvrk::KeyCode::Escape:
                ros::shutdown();
                break;
        
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(_window_name);
                break;
        
            case dvrk::KeyCode::S:
                save_current_image();
                break;
        
            default: break;
        }
    }

    void save_current_image() {
        // Make sure to lock early so that the image doesn't change.
        // If there are many images in the folder already it might take a while to find a free name
        // and if the image is updated in the meantime we won't be saving the right image.
        std::lock_guard<std::mutex> image_lock(_current_image_guard);
        
        // We won't take the picture if the image is not stable. We just check if the previous
        // image is close enough to the current image.
        float image_change = compute_average_difference(
            _previous_image.getImage(), _current_image.getImage());
        if (_noise_threshold > 0 && image_change > _noise_threshold) {
            _last_message = "Noise threshold exceeded (" + std::to_string(image_change) + " > " + std::to_string(_noise_threshold) + ")";
            _last_message_is_error = true;
            return;
        }
        
        if (!_current_image.hasChessboard()) {
            _last_message = "Cannot save image where the chessboard is not present";
            _last_message_is_error = true;
            ROS_WARN_STREAM(_last_message.c_str());
            return;
        }

        const auto image_dir = dvrk::getROSHome() / _camera_name;
        if (fs::create_directories(image_dir)) {
            ROS_INFO_STREAM("Created directory " << image_dir);
        }
        
        // try to find a name that isn't already in use for the new image file.
        for (static int image_number = 0; image_number < 1000; ++image_number) {
            const auto image_name{ _image_name_prefix + "_" + std::to_string(image_number) + ".png" };
            const auto image_filename = image_dir / image_name;
            
            if (!fs::exists(image_filename)) {
                if (!cv::imwrite(image_filename.string(), _current_image.getImage())) {
                    std::cerr << "Error: Unable to write image to " << image_filename << std::endl;
                    exit(-1);
                }
                
                _flash_screen_green = true;
                _last_message = "Saved image to "s + image_filename.string();
                _last_message_is_error = false;
                ROS_INFO_STREAM(_last_message.c_str() << "\n" << "Noise level was: " << image_change) ;
                
                return;
            }
        }

        std::cerr << "Error: Unable to find a name for the image." << std::endl;
        exit(-1);
    }
    
    void set_noise_threshold(const float threshold) { _noise_threshold = threshold; }

private: /* Members */

    const std::string _window_name{ "Image View" };
    const std::string _image_name_prefix;
    const std::string _camera_name;

    ros::NodeHandle _nh;
    dvrk::SimpleCamera _camera;
    
    float _noise_threshold = -1.0f;

    /* State Variables */
    bool _is_window_created = false;
    bool _flash_screen_green = false;

    std::mutex _current_image_guard;
    dvrk::CalibrationImage _current_image;
    dvrk::CalibrationImage _previous_image;
    
    std::string _last_message{ "Press 's' to save image" };
    bool _last_message_is_error = false;

    dvrk::CalibrationPattern _pattern;
};


int main(int argc, char** argv) {

    ros::init(argc, argv, "imview", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    
    float noise_threshold;
    unsigned int pattern_width, pattern_height;
    std::string image_name_prefix, camera_name, pattern_type;

    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help.")
        (
            "name-prefix,n",
            po::value<std::string>(&image_name_prefix)->default_value("image"),
            "The prefix used to name saved images"
        ) (
            "camera-name,c",
            po::value<std::string>(&camera_name)->default_value("stereo-camera"),
            "The name of the camera, used to name the folder in which images are saved"
        )(
            "pattern,p",
            po::value<std::string>()->required(),
            "The pattern spec formatted as type:width:height:feature-size"
        )(
            "noise-threshold,t",
            po::value<float>(&noise_threshold)->default_value(-1.f),
            "The maximum allowed mean difference allowed when saving images"
        );

    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .run();

        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << "Summary:\n"
                      << "    View an image topic in real time. \n"
                      << "\n"
                      << "Usage:\n"
                      << "    rosrun dvrk_common imview image:=/my_image_topic\n"
                      << "\n"
                      << "    The 'image' topic must be remapped to the topic you want to observe."
                      << "\n"
                      << main_opts << std::endl;
            exit(-1);
        }

        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    const auto pattern_spec = cmdline["pattern"].as<std::string>();
    const auto pattern = dvrk::CalibrationPattern::FromString(pattern_spec);
    
    PatternDetector detector(pattern, image_name_prefix, camera_name);
    detector.set_noise_threshold(noise_threshold);
    
    ROS_INFO("Detector Ready ...");
    ros::spin();
    
    return 0;
}
