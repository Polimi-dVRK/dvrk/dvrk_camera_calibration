//
// Created by nearlab on 25/10/17.
//


#include <string>
#include <mutex>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/util.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>

#include <dvrk_camera_calibration/draw.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>
#include <dvrk_common/opencv/util.hpp>

#include "common.hpp"


class ImageReprojector {

public: /* Methods */
    ImageReprojector(const dvrk::CalibrationPattern pattern)
        : _nh(), _camera(_nh), _pattern(pattern)
    {

        _camera.subscribe("image_raw", &ImageReprojector::on_image, this);
        ROS_INFO_STREAM(
            "" << "Ready, waiting for images ... \n"
               << "    Inputs: " << _camera.getTopic() << ", " << _camera.getInfoTopic()
        );
    }
   

public: /* Callbacks */
    
    void on_image(
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel& camera_model
    ) {
    
        const auto image_cv = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
        
        // OpenCV works with pixel sizes. The same text on a 640x480 image an a 1920x1080 resolution
        // will have a very different size. To work around this we scale the images up or down to a
        // consistent size.
        cv::Mat work_image;
        double scale = 1280.0 / image_cv->image.size().width;
        cv::resize(image_cv->image, work_image, cv::Size(), scale, scale);
        
        dvrk::draw_cross(work_image, cv::Point2d(camera_model.cx(), camera_model.cy()), 15, dvrk::Colour::RoyalBlue);
        
        dvrk::CalibrationImage calib_image(work_image, _pattern);
        if (calib_image.hasChessboard()) {
            calib_image.set_intrinsics(
                cv::Mat{ camera_model.intrinsicMatrix() },
                camera_model.distortionCoeffs()
            );
            const auto new_image_points = calib_image.reproject();
        
            draw_errors(work_image, calib_image.getImagePoints(), new_image_points, _error_scale, _error_representation);
            
            const auto err = cv::norm(calib_image.getImagePoints(), new_image_points, cv::NORM_L2);
            const auto rms = sqrt(err * err / new_image_points.size());
            
            cv::putText(work_image, "Mean Reprojection Error: " + std::to_string(rms), cv::Point(10, 40), cv::FONT_HERSHEY_DUPLEX, .5, dvrk::Colour::Purple);
        }
        
        if (!_is_window_created) {
            cv::namedWindow(_window_name, cv::WINDOW_NORMAL);
            _is_window_created = true;
        }
        cv::imshow(_window_name, work_image);

        
        switch(dvrk::waitKey(1)) {
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(_window_name);
                break;
        
            case dvrk::KeyCode::C:
                _error_representation = dvrk::ErrorRepresentation::Crosses;
                break;
        
            case dvrk::KeyCode::L:
                _error_representation = dvrk::ErrorRepresentation::Lines;
                break;
        
            case dvrk::KeyCode::Plus:
                _error_scale += .1;
                break;
        
            case dvrk::KeyCode::Minus:
                _error_scale -= .1;
                break;
        
            case dvrk::KeyCode::Q:
            case dvrk::KeyCode::Escape:
                _camera.unsubscribe();
                ros::shutdown();
        
            default: break;
        }
    }


private: /* Members */
    
    const std::string _window_name{ "Test Window" };
    const std::string _image_name_prefix;
    
    ros::NodeHandle _nh;
    dvrk::SimpleCamera _camera;
    
    /* State Variables */
    bool _is_window_created = false;
    double _error_scale = 1.0;
    dvrk::ErrorRepresentation _error_representation = dvrk::ErrorRepresentation::Crosses;
    
    dvrk::CalibrationPattern _pattern;
};


int main(int argc, char** argv) {
    
    ros::init(argc, argv, "imview", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    
    unsigned int pattern_width, pattern_height;
    std::string image_name_prefix, pattern_type;
    
    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help.")
        (
            "width,w", 
            po::value<unsigned int>(&pattern_width)->required(),
            "The width of the pattern"
        ) (
            "height,h", 
            po::value<unsigned int>(&pattern_height)->required(),
            "The height of the pattern"
        );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << "Summary:\n"
                      << "    View an image topic in real time. \n"
                      << "\n"
                      << "Usage:\n"
                      << "    rosrun dvrk_camera_calibration reproject_realtime -w 9 -h 7 -s .02 image:=/my_image_topic\n"
                      << "\n"
                      << "    The 'image' topic must be remapped to the topic you want to observe."
                      << "\n"
                      << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    cv::Size pattern_size { static_cast<int>(pattern_height), static_cast<int>(pattern_width) };
    dvrk::CalibrationPattern pattern("chessboard", pattern_size, 1.0); 
    ImageReprojector detector(pattern); 
    
    ROS_INFO("Detector Ready ...");

    ros::spin();

    return 0;
}
