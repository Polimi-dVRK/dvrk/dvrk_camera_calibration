
// Created by tibo on 20/10/17.
//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cctype>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

unsigned int mm_to_px(const float mm, const unsigned int dpi = 96) {
    return static_cast<unsigned int>(std::round(mm * dpi / 25.4f));
}

unsigned int pt_to_px(const float pt, const unsigned int dpi = 96) {
    return std::max(
        static_cast<unsigned int>(std::round(pt * dpi / 72.0f)), 1u
    );
}

inline float px_to_mm(const int px, const unsigned int dpi = 96) {
    return px * 25.4f / dpi;
}

cv::Size str_to_paper_size(std::string paper_size_str) {
    std::transform(begin(paper_size_str), end(paper_size_str),  begin(paper_size_str),
        [](unsigned char c){ return std::tolower(c); }
    );

    cv::Size paper_size;
    if (paper_size_str == "none") {
        paper_size = cv::Size(-1, -1);
    } else if (paper_size_str == "a1") {
        paper_size = cv::Size(841, 594);
    } else if (paper_size_str == "a2") {
        paper_size = cv::Size(594, 420);
    } else if (paper_size_str == "a3") {
        paper_size = cv::Size(420, 297);
    } else if (paper_size_str == "a4") {
        paper_size = cv::Size(294, 210);
    } else if (paper_size_str == "a5") {
        paper_size = cv::Size(210, 148);
    } else if (paper_size_str == "a6") {
        paper_size = cv::Size(148, 105);
    } else if (paper_size_str == "a7") {
        paper_size = cv::Size(105, 74);
    } else if (paper_size_str == "a8") {
        paper_size = cv::Size(74, 52);
    } else if (paper_size_str == "a9") {
        paper_size = cv::Size(52, 37);
    } else if (paper_size_str == "a10") {
        paper_size = cv::Size(37, 26);
    } else {
        size_t found_idx = paper_size_str.find('x');
        if (found_idx == std::string::npos) {
            std::cerr << "Error: Invalid paper size specifier.\n"
                      << "The paper-size option expects either a standard A-series (A1, A2, ... "
                      << "A10) format or a size specified as WxH." << std::endl;
            exit(-1);
        }
        try {
            paper_size = cv::Size(
                std::stoi(paper_size_str.substr(0, found_idx)),
                std::stoi(paper_size_str.substr(found_idx+1, std::string::npos))
            );
        } catch (const std::exception& ex) {
            std::cerr << "Error: Invalid paper size specifier.\n"
                      << "The paper-size option expects either a standard A-series (A1, A2, ... "
                      << "A10) format or a size specified as WxH." << std::endl;
            exit(-1);
        }
    }
    
    return paper_size;
}

int main(int argc, char** argv) {
    
    unsigned int pattern_width, pattern_height;
    boost::filesystem::path pattern_file_name;
    
    po::positional_options_description positional_opts;
    positional_opts.add("filename", -1);
    
    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help.")
        (
            "filename,f", 
            po::value<boost::filesystem::path>(&pattern_file_name)->required(), 
            "The name to save the generated pattern as"
        ) (
            "width,w", 
            po::value<unsigned int>(&pattern_width)->default_value(9), 
            "The width of the pattern"
        ) (
            "height,h", 
            po::value<unsigned int>(&pattern_height)->default_value(6), 
            "The height of the pattern"
        ) (
            "square-size,s", 
            po::value<float>()->default_value(10), 
            "The size of squares in mm"
        ) (
            "paper-size,p", 
            po::value<std::string>()->default_value("none"),
            "The size of the paper you intend to print the board on. Set it to none if you do not intend to print the board"
        ) (
            "margin,m", 
            po::value<unsigned int>()->default_value(10), 
            "The minimal amount of whitespace around the board in mm"
        ) (
            "font-size,f", 
            po::value<float>()->default_value(8),
            "The font size of the legend text on the image"
        ) (
            "arrange,a",
            po::value<std::string>(),
            "Arrange the generated boards on a sheet"
        );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .positional(positional_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            // http://www.radmangames.com/programming/how-to-use-boost-program_options
            std::cout << /* Better Usage */ main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name() << "\" missing from command line arguments"
                  << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"" << std::endl << main_opts << std::endl;
        exit(-1);
    }
    
    cv::Size paper_size = str_to_paper_size(cmdline["paper-size"].as<std::string>());
    
    const int blocksize_px = mm_to_px(cmdline["square-size"].as<float>(), 300);
    const int margin_px = mm_to_px(cmdline["margin"].as<unsigned int>(), 300);
    
    // We add 1 to the pattern width and height since the size of chessboard is the number of inner
    // corners along a row a column.
    // For example, if we draw n blocks we have a total of n + 1 corners the first and last of which
    // are 'outer' corners. The total number of 'inner' corners is thus n - 1.
    // If we want a wxh pattern we must make rows w+1 squares long and columns h+1 squares tall.
    cv::Size img_size(
        (pattern_width + 1) * blocksize_px + margin_px * 2,
        (pattern_height + 1) * blocksize_px + margin_px * 2
    );
    
    cv::Scalar White(255, 255, 255), Black(0, 0, 0);    
    cv::Mat pattern(img_size, CV_8UC3, White);

    
    for (auto row = 0; row < pattern_height + 1; ++row) {
        // Make sure that the start colour of rows alternates
        bool white_square = (row % 2 == 0);
        
        for (auto col = 0; col < pattern_width + 1; ++col) {
            cv::Rect square_roi(
                (col * blocksize_px) + margin_px,
                (row * blocksize_px) + margin_px,
                blocksize_px,
                blocksize_px
            );
            cv::Mat this_square = pattern(square_roi);
            
            this_square = white_square ? Black : White;
            white_square = !white_square; // Alternate between black and white squares
        }
    }
    
    // Add board legend
    std::ostringstream legend;
    legend << "Chessboard " << pattern_width << "x" << pattern_height << ", "
        << std::fixed << std::setprecision(1) << px_to_mm(blocksize_px, 300) << "mm Pitch";
    
    // We need to scale the font size down since OpenCV wants it as a multiple of the original size
    // of the font. The multipluier was guessed bsaed on what "looks good".
    const auto font_size = pt_to_px(cmdline["font-size"].as<float>(), 300) / 24.f;
    const cv::Point text_pos{margin_px, static_cast<int>(margin_px * .75)};
    cv::putText(pattern, legend.str(), text_pos, cv::FONT_HERSHEY_DUPLEX, font_size, Black);
    
    // Try to put the generated pattern on the larger image.
    // If a paper size was set to "none", the default value, then the paper size is negative.
    
    if (paper_size.width > 0) {
        cv::Size paper_size_px(
            mm_to_px(paper_size.width, 300),
            mm_to_px(paper_size.height, 300)
        );
    
        cv::Size pattern_size = pattern.size();
        if (pattern_size.width > paper_size_px.width
            || pattern_size.height > paper_size_px.height) {
            std::cerr << "Error: The selected paper size is smaller than the generated pattern. \n"
                      << "\tPattern Size: "
                      << px_to_mm(pattern_size.width, 300) << "mm x "
                      << px_to_mm(pattern_size.height, 300) << "mm\n"
                      << "\tPaper Size: "
                      << px_to_mm(paper_size_px.width, 300) << "mm x "
                      << px_to_mm(paper_size_px.height, 300) << "mm" << std::endl;
            exit(-1);
        }
    
        cv::Mat paper = cv::Mat(paper_size_px, CV_8UC3, White);
        cv::Rect pattern_roi(
            (paper_size_px.width - pattern_size.width) / 2,
            (paper_size_px.height - pattern_size.height) / 2,
            pattern_size.width,
            pattern_size.height
        );
    
        pattern.copyTo(paper(pattern_roi));
        pattern = paper;
    }
    
    // Repeat the pattern the requested number of times
    
    if (cmdline.count("arrange")) {
        cv::Size arrangement;
        
        std::string arrangement_str = cmdline["arrange"].as<std::string>();
        size_t found_idx = arrangement_str.find('x');
        if (found_idx == std::string::npos) {
            std::cerr << "Error: Invalid arrangement specifier.\n"
                      << "The \"--arrange\" option expects a size specified as WxH." << std::endl;
            exit(-1);
        }
        try {
            arrangement = cv::Size(
                std::stoi(arrangement_str.substr(0, found_idx)),
                std::stoi(arrangement_str.substr(found_idx + 1, std::string::npos))
            );
        }  catch (const std::exception& ex) {
            std::cerr << "Error: Invalid arrangement specifier.\n"
                      << "The \"--arrange\" option expects a size specified as WxH." << std::endl;
            exit(-1);
        }
        
        cv::Size full_sheet_size(
            pattern.cols * arrangement.width,
            pattern.rows * arrangement.height
        );
        cv::Mat full_paper(full_sheet_size, pattern.type());
    
        for (int rows = 0; rows < arrangement.height; ++rows) {
            for (int cols = 0; cols < arrangement.width; ++cols) {
                cv::Rect pattern_roi(
                    pattern.cols * cols, pattern.rows * rows,
                    pattern.cols       , pattern.rows
                );
                pattern.copyTo(full_paper(pattern_roi));
            }
        }
        
        pattern = full_paper;
    }
    
    try {
        if (pattern_file_name.extension().empty()) {
            pattern_file_name += ".png";
        }
        
        std::cout << "Saving " << pattern_width << "x" << pattern_height << " ("
                  << cmdline["square-size"].as<float>() << "mm) chessboard to " << pattern_file_name << " ... ";
        cv::imwrite(pattern_file_name.string(), pattern);
    } catch (const cv::Exception& ex) {
        std::cerr << "Unable to save the genrated pattern to " << pattern_file_name
                  << ". The error was: " << ex.what() << std::endl;
        return 1;
    }
    
    std::cout << "Done\n\n"
              << "Note: The pitch size displayed on the chessboard assumes that the image is "
              << "printed with no scaling at 300dpi. To achieve the best results convert it to "
              << "pdf format first."<< std::endl;
    return 0;
}

