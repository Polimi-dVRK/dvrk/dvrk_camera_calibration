//
// Created by tibo on 06/11/17.
//

#include <iostream>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <dvrk_camera_calibration/util.hpp>
#include <dvrk_camera_calibration/draw.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>

#include "common.hpp"

const std::string cv_window_name{ "Reprojected Points" };

int main(int argc, char** argv) {
    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help.")
        (
            "image,i",
            po::value<std::string>()->required(),
            "The image on which to detect and reproject the calibration target."
        ) (
            "calibration-file,c",
            po::value<std::string>()->required(),
            "The file from which to rread the intrinsic camera calibration parameters"
        ) (
            "width,w", 
            po::value<unsigned int>()->default_value(9),
            "The width of the pattern"
        ) (
            "height,h", 
            po::value<unsigned int>()->default_value(6),
            "The height of the pattern"
        ) (
            "size,s", 
             po::value<float>()->default_value(10),
             "The size of squares in mm"
        );
    
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .run();
        
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << main_opts << std::endl;
            exit(-1);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name()
                  << "\" missing from command line arguments" << std::endl;
        exit(-1);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"\n" << main_opts
                  << std::endl;
        exit(-1);
    }
    
    // Load calibration data
    cv::Mat camera_matrix, distortion_coefficients;
    fs::path calibration_file{ cmdline["calibration-file"].as<std::string>() };
    dvrk::load_intrinsics(calibration_file, camera_matrix, distortion_coefficients);
    
    std::cout << "Intrinsics: " << camera_matrix << distortion_coefficients << std::endl;
    
    const cv::Size pattern_size{ cmdline["width"].as<unsigned int>(), cmdline["height"].as<unsigned int>() };
    dvrk::CalibrationPattern pattern(
        "chessboard", pattern_size, cmdline["size"].as<float>()
    );
    
    fs::path image_file{ fs::absolute(cmdline["image"].as<std::string>()) };
    if (!fs::exists(image_file)) {
        std::cerr << "Error: " << image_file << " does not exist." << std::endl;
    }
    
    std::cout << "Loading " << image_file << " ...\n";
    
    cv::Mat raw_image = dvrk::read_image(image_file);
    cv::Mat work_image;
    
    // OpenCV works with pixel sizes. The same text on a 640x480 image an a 1920x1080 resolution
    // will have a very different size. To work around this we scale the images up or down to a
    // consistent size.
    double scale = 1280.0 / raw_image.size().width;
    cv::resize(raw_image, work_image, cv::Size(), scale, scale);
    
    dvrk::CalibrationImage image(work_image.clone(), pattern);
    image.set_intrinsics(camera_matrix, distortion_coefficients);
    
    std::cout << "Reprojecting points ...\n";
    const auto new_image_points = image.reproject();

    std::cout << "All done, press 'Q' or 'Esc' to exit\n";
    cv::namedWindow(cv_window_name, cv::WINDOW_NORMAL);
    
    bool quit = false;
    
    cv::Mat canvas;
    float error_scale = 1.0f;
    dvrk::ErrorRepresentation  error_representation = dvrk::ErrorRepresentation::Crosses;
    
    while (!quit) {
        canvas = work_image.clone();
        dvrk::draw_errors(canvas, image.getImagePoints(), new_image_points, error_scale, error_representation);
        cv::imshow(cv_window_name, canvas);
        
        switch(cv::waitKey(40)) {
            case 99: /* KeyCode: C */
                error_representation = dvrk::ErrorRepresentation::Crosses;
                break;
                
            case 108: /* KeyCode: L */
                error_representation = dvrk::ErrorRepresentation::Lines;
                break;
                
            case 43: /* KeyCode: + */
                error_scale += .1;
                break;
    
            case 45: /* KeyCode: - */
                error_scale -= .1;
                break;
                
            case 113: /* KeyCode: Q */
            case 27: /* KeyCode: Escape */
                quit = true;
                break;
                
            default: break;
        }
    }
    
    return 0;
}
