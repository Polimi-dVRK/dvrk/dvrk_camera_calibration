
=======================
dvrk_camera_calibration
=======================

This package is a collection of tools to obtain the best possible calibrations of the intrinsic and extrinsic camera parameters. It provides: 

- ``mkpattern``, a small program to generate calibration patterns of configurable size,
- ``calibrate``, a program that reads a set of images and computes the intrinsic calibration parameters discarding outlier images,
- ``imview``, an optional ROS node to view and save images. 

mkpattern
---------

The ``mkpattern`` utility is responsible for creating a wide array of calibration patterns. At the moment only chessboards are supported but more are planned.

Printing the best possible patterns
***********************************

The tool outputs a `.png` image but if you should not use it to calibrate directly. It is extremely difficult to convince a printer to print an image without first rescaling it. The workaround is to save the image as a `.pdf`. When printing a `.pdf` it is usually quite simple to disable printer scaling.

My workflow for this is:

1. Open new chessboard image in GIMP
2. Create a new image and select the *A4* template
3. Copy the chessboard image onto the new A4 page
4. Export the A4 page as `.pdf`
5. Print `.pdf`

Alternatively, if you have 'imagemagick` installed the conversion to `.pdf` can be done by running ::

    convert -quality 100 -density 300x300 -resize 100% test-pattern.png test-pattern.pdf

The printed chessboard should be glued to a rigid support. In a pinch cardboard will work but you should look for something less bendy. Ideally you should glue the pattern to a piece of plexiglass.

calibrate
---------


imview
------

``imview`` is an optional ROS node to view and save images from an ``image_transport`` compatible  topic. It listens for images on the ``image`` topic which should be remapped and looks for a calibration target. The characteristics of the target should be passed with command line arguments. Currently only *chessboards* are supported but support for *asymmetric circles* and *charuco* boards is planned. 

An example invocation of imview is: ::

    rosrun dvrk_camera_calibration imview -p "chessboard" -w 9 -h 6 image:=/usb_cam/image_raw

This tells the program to look for *9x6* *chessboard* type targets in images read from ``/usb_cam/image_raw``.


