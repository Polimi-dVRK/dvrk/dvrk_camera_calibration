//
// Created by tibo on 20/08/17.
//


#pragma once

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <opencv2/core/types.hpp>

#include "dvrk_camera_calibration/calibration_pattern.hpp"

namespace dvrk {

    /**
     * This class stores an image and all the information needed when using this image as part of a
     * calibration dataset.
     *
     * The data contained in the class encompasses the image and object points, whether the chessboard
     * was found, the area of the chessboard in the image and any other metrics used tode termine if
     * this image is a good candidate for inclusion in the calibration dataset.
     *
     * This makes it  easier to keep track of which image points belong to which image or keep track of
     * variation in board skew across all the images.
     */
    class CalibrationImage {
    public: /* Types */
        
        /// This offers convenient access to the outer corners of the detected board.
        struct OutsideCorners {
            cv::Point2f top_left, top_right, bottom_right, bottom_left;
        };
        
        /// This offers convenient access to the rotation and translation components of the camera pose.
        struct Pose {
            cv::Mat rotation;// TODO: use proper Matx types
            cv::Mat translation;
        };
        
        struct Params {
            double p_x, p_y, p_size, p_skew;
        };
    
    public: /* methods */
        
        CalibrationImage() {};
        
        /// Constructor, builds a complete #CalibrationImage using the default object points.
        CalibrationImage(
            const cv::Mat& image,
            const CalibrationPattern& pattern,
            const std::string& name = ""
        );
        
        /// Constructor, builds a complete #CalibrationImage
        CalibrationImage(
            const cv::Mat& image,
            const CalibrationPattern& pattern,
            const std::vector<cv::Point3f>& object_points,
            const std::string& name = ""
        );
        
        /// Constructor, builds a complete #CalibrationImage using the default object points loading
        /// the image from file.
        CalibrationImage(
            const std::string& filename,
            const CalibrationPattern& pattern,
            const std::string& name = ""
        );
        
        /// Constructor, builds a complete #CalibrationImage loading the image from file.
        CalibrationImage(
            const std::string &filename,
            const CalibrationPattern &pattern,
            const std::vector<cv::Point3f> &object_points
        );
    
        /// Constructor, builds a complete #CalibrationImage using the default object points loading
        /// the image from file.
        CalibrationImage(
            const fs::path &filename,
            const CalibrationPattern &pattern
        );
    
        /// Constructor, builds a complete #CalibrationImage loading the image from file.
        CalibrationImage(
            const fs::path &filename,
            const CalibrationPattern &pattern,
            const std::vector<cv::Point3f> &object_points
        );
        
        std::vector<cv::Point2f> reproject() const;
        
        /**
         * Make an image showing the detected and reprojected points.
         *
         * Detected points are shown with a green cross whilst reprojected points are shown with a
         * red cross.
         *
         * @param show_detected Whether the detected image points should be shown
         * @param show_reprojected Whether the detected image points should be shown
         */
        cv::Mat show_points(bool show_detected = true, bool show_reprojected = true);
     
    public: /* Getters & Setters */
        
        cv::Size size() const { return _image.size(); }
        
        bool empty() const { return _image.empty(); }
        
        /// Get the image data stored in the object
        const cv::Mat& getImage() const { return _image; }
        
        /// Get the name of the image
        const std::string getName() const { return _name; }
        
        /// Get the specification for the calibration pattern that should be present in the image
        const CalibrationPattern getCalibrationPattern() const { return _pattern; }
        
        /// Get the corners of the calibration pattern in world (3D) coordinates
        const std::vector<cv::Point3f>& getObjectPoints() const { return _object_points; }
        
        /// Get the corners of the calibration pattern in image (2D) coordinates
        const std::vector<cv::Point2f>& getImagePoints() const { return _image_points; }
        
        /// Get the 4 outside corners of the calibration pattern in the image.
        const OutsideCorners& getOutsideCorners() const { return _outside_corners; }
        
        /// Was the calibration pattern found in the image ?
        bool hasChessboard() const { return _chessboard_found; }
        
        /// Get the skew of the calibration pattern in the image
        double getBoardSkew() const { return _board_skew; }
        
        /// Get the size of the calibration pattern in the image
        double getBoardArea() const { return _board_area; }
        
        ///  Get the position of the centroid of the board in the image
        cv::Point2f getBoardCentreOfMass() const { return _board_centre_of_mass; }
        
        /// Set the intrinsic parameters used to compute the pose of the calibration pattern.
        /// When the intrinsics are changed the the relevant data class data is updated too.
        void set_intrinsics(const cv::Mat& camera_matrix, const cv::Mat& distortion_coeffs);
        
        /// Compute the reprojection error in pixels for this image
        double get_reprojection_error() const;
        
        
        /**
         * Compute the reprojection error in millimetres for this image.
         *
         * This is preferrable compared to the pixel value because it is distance independant. The
         * measure in px can be be artifically decreased by simply putting the calibration pattern
         * farther away from the camera but the value in mm is the same whatever the distance.
         */
        double get_reprojection_error_mm() const;
        
        void setPose(cv::Mat& rotation, cv::Mat& translation);
        
    public: /* Conversion Operators */
        
        operator cv::Mat () const { return _image; }
    
        operator cv::Mat const () const { return _image; }


    protected:
        
        /**
         * Do the heavy(-ish) lifting of actually setting up the object. All the constructors just
         * delegate to this method.
         *
         * @param image The image to store
         * @param pattern The calibration pattern details to use with this image
         * @param object_points The points of the calibration pattern in world space
         */
        void initialise(
            const cv::Mat& image,
            const CalibrationPattern& pattern,
            const std::vector<cv::Point3f>& object_points,
            const std::string& name
        );
        

        /**
         * Generate a set of object points where the calibration pattern is placed on a horizontal plane
         * at z = 0.
         *
         * @param pattern The pattern information
         * @return The position of each corner (feature) of the pattern
         */
        std::vector<cv::Point3f> defaultObjectPoints(const CalibrationPattern& pattern);
        
        CalibrationImage::OutsideCorners computeBoardOuterCorners(
            const std::vector<cv::Point2f>& image_points,
            const CalibrationPattern& pattern
        );
        
        /**
         * This function is the "meat" of the class. The method will process the image to locate the
         * image points and compute any required metrics on the image.
         */
        void locate_chessboard();
        
        /**
         * Get the skew of the board.
         *
         * The skew of the board is a measure of how much the board is rotated with respect to the
         * image plane. If the board and the image plane are parallel the skew will be exactly 1,
         * as the board is rotated away the skew decreases.
         *
         * @return the skew of the board
         */
        double computeBoardSkew();
        
        /**
         * Compute the area of the chessboard in image coordintes. The area of the board can be used
         * as a proxy for how far the board is from the camera.
         *
         * @return The area of the chessboard in image space
         */
        double computeBoardArea();
        
        cv::Point2f computeBoardCentroid();
        
        /// Compute the pose of the calibration pattern in the image using the stored intrinsics.
        void compute_pose();

    private:
        
        /// The actual image data.
        cv::Mat _image;
        
        cv::Mat  _camera_matrix;
        cv::Mat _distortion_coeffs;
        
        /// The name should be a unique identifier for the image.
        std::string _name;
        
        // Information about the calibration pattern used in this image.
        CalibrationPattern _pattern;
        
        /// Whether or not a complete chessboard was detected in the image.
        bool _chessboard_found = false;
        
        /// The positions of the corners in object space (i.e. world space).
        std::vector<cv::Point3f> _object_points;
        
        /// The positions of the corners in image space.
        std::vector<cv::Point2f> _image_points;
    
        /// The positions of the corners in image space as estimated by the reproject() function
        mutable std::vector<cv::Point2f> _reprojected_image_points;
    
        /// the positions of the top left, top right, bottom left and bottom right corners of the
        /// chessboard in image space.
        OutsideCorners _outside_corners;
        
        /// A measure of the parallelism between the board plane and the image plane, see
        /// CalibrationImage::getBoardSkew for details.
        double _board_skew = -1.0;
        
        /// The area of the chessboard in px^2, see CalibrationImage::getBoardArea for details
        double _board_area = -1.0;
        
        /// The position of the centre of the board in image space
        cv::Point2f _board_centre_of_mass;
        
        /// The computed pose of the camera in world space
        Pose _pose;
    };
}
