//
// Created by tibo on 13/11/17.
//

#pragma once

#include <vector>

#include "camera_params.hpp"
#include "calibration_image.hpp"

namespace dvrk {
    
    class StereoCalibrationDataset {
    public: /* Methods */
        StereoCalibrationDataset(
            const std::vector<CalibrationImage>& left_images,
            const std::vector<CalibrationImage>& right_images
        );
        
        void calibrate();
    
        void save_calibration(
            const fs::path& left_filename,
            const fs::path& right_filename
        );
    
        void set_calibration_flags(int flags) { _calibration_flags = flags; }
        int get_calibration_flags() const { return _calibration_flags; }
        
        void set_left_camera_calibration(const CameraParams& left_camera_params);
        void set_right_camera_calibration(const CameraParams& right_camera_params);
        void fix_intrinsics(bool fix) { _fix_camera_intrinsics = fix; }
        
    private: /* Methods */
        
    
    private: /* Members */
    
        int _calibration_flags{ 0 };
        bool _fix_camera_intrinsics;
        
        std::vector<CalibrationImage> _left_images;
        std::vector<CalibrationImage> _right_images;
        
        CameraParams _left_camera_params;
        CameraParams _right_camera_params;
    };
}
