//
// Created by tibo on 23/10/17.
//


#pragma once

#include <set>
#include <boost/filesystem.hpp>

#include <opencv2/core/mat.hpp>

namespace dvrk {

    // The list of supported file extensions is taken straight from the OpenCV reference.
    // See: https://docs.opencv.org/3.2.0/d4/da8/group__imgcodecs.html#ga288b8b3da0892bd651fce07b3bbd3a56
    const std::set<std::string> supported_image_formats{
        ".bmp", ".dib", "j.peg", ".jpg", ".jp2", ".png", "w.ebp", ".pbm", ".pgm", ".ppm", ".pxm",
        ".pnm", ".tiff", ".tif", ".exr", ".hdr", ".pic"
    };
    
    /**
     * Is the file an image file. This compares the file extension with a whitelist of supported
     * image files: dvrk::supported_image_formats.
     *
     * @param file The file to check
     * @return Whther or not the extension of the file is in the dvrk::supported_image_files list.
     */
    bool is_image_file(const boost::filesystem::path &file);
    
    
    /**
     * Read an image from a file.
     *
     * @param filename The file to read the image from
     * @return The image that was read
     */
    cv::Mat read_image(const boost::filesystem::path &filename);
    
    
    cv::FileStorage load_intrinsics(
        boost::filesystem::path intrinsics_file,
        cv::Mat& camera_matrix,
        cv::Mat& distortion_coefficients
    );
}
