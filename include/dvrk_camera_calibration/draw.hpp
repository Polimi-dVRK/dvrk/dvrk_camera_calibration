//
// Created by tibo on 22/12/17.
//

#pragma once

#include <opencv2/core.hpp>

namespace dvrk {
    
    enum class ErrorRepresentation {
        Crosses, Lines
    };

    /**
     * Given the \c detected_image_points (from cv::findChessboard) and the \c reprojected_image_points,
     * show the reprojection error on the image using a variable error scaling factor.
     *
     * @param canvas Te image to draw on.
     * @param detected_image_points The image points detected with cv::findChessboard
     * @param reprojected_image_points
     *  The image points reprojected using the intrinsic and extrinsic camera parameters
     * @param error_scale
     *  A scaling factor for the error. Values greater than one will increase the distance between the
     *  detected abd reprojected image points.
     * @param error_representation
     *  The way the error should be show. With ErrorRepresentation::Crosses the detected and reprojected
     *  points are shown as crosses of different colours, with ErrorRepresentation::Lines a line is
     *  drawn from the detected to the reprojected point.
     * @param marker_size
     *  The size of the crosses used to show the position of the image points in pixels.
     */
    void draw_errors(
        cv::Mat& canvas,
        const std::vector<cv::Point2f>& detected_image_points,
        const std::vector<cv::Point2f>& reprojected_image_points,
        const float error_scale,
        const ErrorRepresentation error_representation,
        const int marker_size = 10
    );

    /**
     * Draw a cross on an image at the specified position.
     *
     * @param image The image on which to draw
     * @param position The position at which to place the centre of the cross
     * @param size The size of the cross in pixels
     * @param colour The colour of the cross
    */
    void draw_cross(cv::Mat& image, cv::Point position, int size, cv::Scalar colour);

}
