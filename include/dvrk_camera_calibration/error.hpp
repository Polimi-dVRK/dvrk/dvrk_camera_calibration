//
// Created by nearlab on 23/10/17.
//

#pragma once

#include <boost/exception/all.hpp>

namespace dvrk {
    
    struct exception_base: virtual std::exception, virtual boost::exception {};
    
    struct runtime_error: virtual exception_base {};
    typedef boost::error_info<struct tag_errmsg, const std::string> errmsg;
    
    struct out_of_range_error: virtual runtime_error {};
    struct type_error: virtual runtime_error {};
    
    struct key_error: virtual runtime_error{};
    typedef boost::error_info<struct tag_errkey, const std::string> errkey;
    
    struct io_error: virtual runtime_error {};
    
    struct file_error: virtual io_error {};
    typedef boost::error_info<struct tag_errfile, const std::string> errfile;
    
    struct not_a_file_error: virtual file_error {};
    struct file_open_error: virtual file_error {};
    struct file_read_error: virtual file_error {};
    struct file_parse_error: virtual file_error {};
    
    struct empty_image_error: virtual runtime_error {};
    struct size_error: virtual runtime_error {};
}
