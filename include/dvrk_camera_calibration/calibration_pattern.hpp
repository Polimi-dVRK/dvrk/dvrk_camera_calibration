//
// Created by tibo on 23/10/17.
//

#pragma once

#include <string>
#include <regex>
#include <opencv2/core/types.hpp>

#include "dvrk_camera_calibration/error.hpp"

namespace dvrk {
    
    struct CalibrationPattern {
        enum class Type {
            Unspecified, Chessboard,
        };
        
        /**
         * Extract a pattern specification from a string.
         *
         * The expected format is:
         *
         *  type:width:height:feature-size
         *
         * Where type is a string, width and height are int and feature size if float.
         *
         * @param pattern_spec The string containg the pattern specification
         * @return The parsed pattern
         */
        static CalibrationPattern FromString(const std::string pattern_spec) {
            std::regex pattern_matcher(R"((\w*):(\d+):(\d+):(\d*\.\d+|\d+))");
            std::smatch matches;
            
            CalibrationPattern pattern;
            if (std::regex_match(pattern_spec, matches, pattern_matcher)) {
                if (matches[1].str() == "chessboard")
                    pattern.type = Type::Chessboard;
                
                pattern.size.width = std::stoi(matches[2].str());
                pattern.size.height = std::stoi(matches[3].str());
                pattern.feature_size = std::stof(matches[4].str());
            }
            
            return pattern;
        }
        
        static CalibrationPattern FromFileStorage(cv::FileNode fs) {
            CalibrationPattern pattern;
            
            std::string type_str = static_cast<std::string>(fs["type"]);
            if (type_str == "unspecified") pattern.type = Type::Unspecified;
            else if (type_str == "chessboard") pattern.type = Type::Chessboard;
            else {
                BOOST_THROW_EXCEPTION(out_of_range_error()
                    << errmsg(type_str + " is not a valid calibration pattern type."));
            }
            
            if (fs["pattern_width"].isNone())
                BOOST_THROW_EXCEPTION(out_of_range_error()
                    << errmsg("The 'pattern_width' key is missing from the pattern description."));
            if (!fs["pattern_width"].isInt())
                BOOST_THROW_EXCEPTION(type_error()
                    << errmsg("The 'pattern_width' value should be a positive integer."));
            pattern.size.width = static_cast<int>(fs["pattern_width"]);
    
            if (fs["pattern_height"].isNone() and !fs["pattern_height"].isInt())
                BOOST_THROW_EXCEPTION(out_of_range_error()
                    << errmsg("The 'pattern_height' key is missing from the pattern description."));
            if (!fs["pattern_height"].isInt())
                BOOST_THROW_EXCEPTION(type_error()
                    << errmsg("The 'pattern_height' value should be a positive integer."));
            pattern.size.height = static_cast<int>(fs["pattern_height"]);
    
            if (fs["feature_size"].isNone())
                BOOST_THROW_EXCEPTION(out_of_range_error()
                    << errmsg("The 'feature_size' key is missing from the pattern description."));
            if (!fs["feature_size"].isReal())
                BOOST_THROW_EXCEPTION(type_error()
                    << errmsg("The 'feature_size' value should be a real number."));
            pattern.feature_size = static_cast<float>(fs["feature_size"]);
            
            return pattern;
        }
        
        CalibrationPattern() {};
        
        CalibrationPattern(const std::string pattern_type, cv::Size pattern_size, float feature_size)
            : size(pattern_size), feature_size(feature_size)
        {
            if (pattern_type == "chessboard") type = Type::Chessboard;
        }
        
        /**
         * Get the index of the specified corners in the vector of corners returned by
         * cv::findChessboardCorners.
         */
        inline size_t get_corner_idx(unsigned int row, unsigned int col) {
            return static_cast<size_t>(row * size.width) + col;
        }
        
        Type type = Type::Unspecified;
        
        /**
         * The number of repetitions of the elements in the pattern along the \c and \c y directions.
         * In the caes of a Chessboard for example this is the number of square along side of the board.
         */
        cv::Size2i size = {0, 0};
        
        /*
         * The size of each feature of the pattern. In the case of the chessboard this is the size of
         * the individual squares.
         */
        float feature_size = 0.f;
    };
    
}
