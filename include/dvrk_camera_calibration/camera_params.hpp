//
// Created by tibo on 14/11/17.
//

#pragma once

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <opencv2/core.hpp>
#include <opencv2/core/persistence.hpp>

namespace dvrk {
    
    struct CameraParams {
    public :/* Members */
        
        // Single camera data
        cv::Mat camera_matrix;
        cv::Mat distortion_coefficients;

        // Stereo camera data 
        cv::Mat rectification_matrix;
        cv::Mat projection_matrix;
        
        // Other data
        std::string calibration_date;
        std::string calibration_tool;
        std::string distortion_model = "plumb_bob";
        std::string camera_name;
        
        double reprojection_error = -1.;
        int frame_count = -1;
        int stereo_frame_count = -1;
        
        cv::Size image_size{-1, -1};
        
    public: /* Static Methods */
    
        static CameraParams FromFile(cv::FileStorage& file);
        
        static CameraParams FromFile(const fs::path& filename);
        
    public: /* Class Methods */
        
        void to_file(cv::FileStorage& file);
        
        void to_file(const fs::path& filename);
    };
    
}
