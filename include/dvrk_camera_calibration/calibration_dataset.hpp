//
// Created by nearlab on 25/10/17.
//

#pragma once

#include "camera_params.hpp"
#include "calibration_image.hpp"

namespace dvrk {

class CalibrationDataset {

public: /* Methods */
    
    CalibrationDataset(double threshold = .005);

    explicit CalibrationDataset(const std::vector<CalibrationImage>& images, double threshold = 1.0);
    
    void add(CalibrationImage image);
    size_t size() const { return _initial_imageset.size(); }
    
    void set_calibration_flags(int flags);
    int get_calibration_flags() const { return _calibration_flags; }
    
    void calibrate();
    
    void save_details(const fs::path &filename);
    
    void save_extra_details(const fs::path &filename);
    
    void save_calibration(const fs::path &filename);
    
    const std::vector<CalibrationImage> get_images() const { return _initial_imageset; }
    
    void set_min_iteration_delta(double threshold) { _min_iteration_change = threshold; };
    double get_min_iteration_delta() const { return _min_iteration_change; }
    
    void set_min_imageset_size(size_t min_imageset_size) { _min_imageset_size = min_imageset_size; }
    size_t get_min_imageset_size() const { return _min_imageset_size; }
    
protected: /* Methods */
    
    std::vector<double> iterate_once(
        std::vector<CalibrationImage> &images_in_use,
        cv::Mat &camera_matrix,
        cv::Mat &distortion_params
    );
    
    void update_error_stats(
        const std::vector<CalibrationImage> &images_in_use,
        const std::vector<double> &errors,
        const cv::Mat& camera_matrix
    );

    void show_iteration_stats(const std::vector<CalibrationImage> &images_in_use);

    bool should_continue(const std::vector<CalibrationImage>& images_in_use);
    
private: /* Members */
    
    /**
     * A threshold for the iteration delta.
     *
     * When the delta between the mean error of the current iteration and the previous is less
     * than this value the algorithm stops.
     */
    double _min_iteration_change = 1.;
    
    /**
     * When calibrating we start by discrding all the images with a reprojection error of more
     * this value.
     *
     * Since we use subpixel refinement we can be pretty confident that the errors of more than 1
     * pixel are too much.
     */
    double _hard_error_cutoff = 1.;
    
    
    size_t _min_imageset_size = 20;
    std::string _camera_name = "default camera";
    
    int _calibration_flags{ 0 };
    
    std::vector<CalibrationImage> _initial_imageset;
    
    std::vector<CalibrationImage> _final_imageset;
    
    std::vector<double> _fx;
    std::vector<double> _fy;
    std::vector<double> _cx;
    std::vector<double> _cy;
    std::vector<double> _max_error;
    std::vector<double> _mean_errors;
    std::vector<double> _std_deviations;
    
    std::map< std::string, std::vector<double> > _all_reprojection_errors;
    
    CameraParams _camera_params;
};

}
