//
// Created by tibo on 08/01/18.
//

#pragma once

#include <cmath>
#include <opencv2/core.hpp>

struct Line {
    
    Line(cv::Point2f p1, cv::Point2f p2) : p1(p1), p2(p2) {}
    
    /// \c p1 and \c p2 are two points through which the line passes
    cv::Point2f p1, p2;
};

float length(Line line) {
     return std::sqrt( std::pow(line.p2.y - line.p1.y, 2) + std::pow(line.p2.x - line.p1.x, 2) );
}

template <typename Point>
float distance(Line line, Point point) {
    return std::abs(
         (line.p2.y - line.p1.y) * point.x - (line.p2.x - line.p1.x) * point.y
        + line.p2.x * line.p1.y - line.p2.y * line.p1.y
    ) / length(line);
}