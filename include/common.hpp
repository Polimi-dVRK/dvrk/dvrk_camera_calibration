//
// Created by tibo on 09/11/17.
//

#pragma once

#include <vector>
#include <opencv2/core.hpp>

#include <dvrk_camera_calibration/calibration_dataset.hpp>

float compute_average_difference(const cv::Mat& lhs,  const cv::Mat& rhs);

/**
 *
 * @param imagelist
 * @param pattern
 * @param base_path
 * @return
 */
dvrk::CalibrationDataset load_images(
    const cv::FileNode& imagelist,
    const dvrk::CalibrationPattern& pattern,
    const fs::path& base_path
);
